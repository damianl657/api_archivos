-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-07-2016 a las 21:06:12
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nodos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventosusers`
--

CREATE TABLE `eventosusers` (
  `id` int(11) NOT NULL,
  `eventos_id` int(11) NOT NULL,
  `users_id` bigint(20) UNSIGNED NOT NULL,
  `token_id` int(11) NOT NULL,
  `visto` int(11) DEFAULT '0',
  `me_gusta` int(11) NOT NULL,
  `aceptado` int(11) DEFAULT '0',
  `enviado` int(11) NOT NULL DEFAULT '0',
  `medio` varchar(1) NOT NULL,
  `destinatario` int(11) NOT NULL COMMENT 'Contiene los user_id del usuario al que esta dirigido el evento',
  `alta` varchar(1) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaConfirmacion` datetime NOT NULL,
  `fechaVisto` datetime DEFAULT NULL,
  `fechaMegusta` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `eventosusers`
--
ALTER TABLE `eventosusers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `eventosusers`
--
ALTER TABLE `eventosusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
