-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-07-2016 a las 17:20:48
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nodos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventosusers`
--

CREATE TABLE IF NOT EXISTS `eventosusers` (
`id` int(11) NOT NULL,
  `eventos_id` int(11) NOT NULL,
  `users_id` bigint(20) unsigned NOT NULL,
  `token_id` int(11) NOT NULL,
  `visto` int(11) DEFAULT '0',
  `aceptado` int(11) DEFAULT '0',
  `enviado` int(11) NOT NULL DEFAULT '0',
  `medio` varchar(1) NOT NULL,
  `destinatario` int(11) NOT NULL COMMENT 'Contiene los user_id del usuario al que esta dirigido el evento',
  `alta` varchar(1) NOT NULL,
  `fechaCreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaConfirmacion` datetime DEFAULT NULL,
  `fechaVisto` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `eventosusers`
--

INSERT INTO `eventosusers` (`id`, `eventos_id`, `users_id`, `token_id`, `visto`, `aceptado`, `enviado`, `medio`, `destinatario`, `alta`, `fechaCreacion`, `fechaConfirmacion`, `fechaVisto`) VALUES
(80, 68, 20, 0, 0, 1, 1, 'e', 20, 'a', '2016-07-11 12:52:31', NULL, NULL),
(81, 69, 10, 79, 1, 1, 1, 'm', 20, 'a', '2016-07-11 15:17:43', '2016-07-11 17:19:59', '2016-07-11 17:11:33'),
(82, 70, 10, 79, 1, 1, 1, 'm', 20, 'a', '2016-07-11 15:11:34', '2016-07-11 17:20:10', '2016-07-11 17:11:34'),
(83, 71, 10, 79, 1, 1, 1, 'm', 20, 'a', '2016-07-11 15:17:31', '2016-07-11 17:17:31', '2016-07-11 17:11:39');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `eventosusers`
--
ALTER TABLE `eventosusers`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `eventosusers`
--
ALTER TABLE `eventosusers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
