-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2016 at 08:38 PM
-- Server version: 5.5.46
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enodos_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
CREATE TABLE `access` (
  `id` int(11) unsigned NOT NULL,
  `key` varchar(40) NOT NULL DEFAULT '',
  `controller` varchar(50) NOT NULL DEFAULT '',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `anios`
--

DROP TABLE IF EXISTS `anios`;
CREATE TABLE `anios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `niveles_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `anios`
--

INSERT INTO `anios` (`id`, `nombre`, `niveles_id`) VALUES
(1, 'Sala de 3', 1),
(2, 'Sala de 4', 1),
(4, '1° grado', 2),
(5, '2° grado', 2),
(6, '1° año', 3),
(7, '2° año', 3),
(8, '4 año', 4);

-- --------------------------------------------------------

--
-- Table structure for table `cicloa`
--

DROP TABLE IF EXISTS `cicloa`;
CREATE TABLE `cicloa` (
  `id` int(11) NOT NULL,
  `cicloa` int(11) NOT NULL,
  `planesestudio_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cicloa`
--

INSERT INTO `cicloa` (`id`, `cicloa`, `planesestudio_id`) VALUES
(1, 2016, 1);

-- --------------------------------------------------------

--
-- Table structure for table `destino`
--

DROP TABLE IF EXISTS `destino`;
CREATE TABLE `destino` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dias`
--

DROP TABLE IF EXISTS `dias`;
CREATE TABLE `dias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dias`
--

INSERT INTO `dias` (`id`, `nombre`) VALUES
(1, 'Lunes'),
(2, 'Martes'),
(3, 'Miercoles'),
(4, 'Jueves'),
(5, 'Viernes'),
(6, 'Sabado'),
(7, 'Domingo');

-- --------------------------------------------------------

--
-- Table structure for table `divisiones`
--

DROP TABLE IF EXISTS `divisiones`;
CREATE TABLE `divisiones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `orden` int(11) NOT NULL,
  `anios_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `divisiones`
--

INSERT INTO `divisiones` (`id`, `nombre`, `orden`, `anios_id`) VALUES
(1, '1° Grado "A"', 1, 4),
(2, '1° Grado "B"', 2, 4),
(3, '2° Grado "A"', 1, 5),
(4, '2° Grado "B"', 2, 5),
(5, '1 Año "A"', 1, 6),
(6, '1 Año "B"', 2, 6),
(7, '2 Año "A"', 1, 7),
(8, '2 Año "B"', 2, 7),
(9, '4 Año "A"', 1, 8),
(10, '4 Año "B"', 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `especialidades`
--

DROP TABLE IF EXISTS `especialidades`;
CREATE TABLE `especialidades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `especialidades`
--

INSERT INTO `especialidades` (`id`, `nombre`, `orden`) VALUES
(1, 'Economía', 1),
(2, 'Naturales', 2);

-- --------------------------------------------------------

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(145) DEFAULT NULL,
  `confirm` int(11) DEFAULT NULL,
  `fechaCreacion` timestamp NULL DEFAULT NULL,
  `fechaInicio` datetime DEFAULT NULL,
  `descripcion` blob,
  `users_id` bigint(20) unsigned NOT NULL COMMENT 'el que escribe el evento',
  `lugar` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `eventos`
--

INSERT INTO `eventos` (`id`, `titulo`, `confirm`, `fechaCreacion`, `fechaInicio`, `descripcion`, `users_id`, `lugar`) VALUES
(1, 'Se suspenden las clases', 0, '2015-11-06 13:43:42', '2015-12-10 08:00:00', 0x53652073757370656e64656e206c617320636c61736573, 3, 'Colegio'),
(2, 'Hoy salen mas temprano', 1, '2015-11-08 14:25:42', '2015-12-14 08:00:00', 0x486f792073616c656e206d61732074656d7072616e6f, 4, 'Colegio'),
(3, 'Excursión al museo de bellas artes', 0, '2015-11-04 20:31:42', '2015-12-16 10:00:00', 0x45786375727369c3b36e20616c206d7573656f2064652062656c6c6173206172746573, 5, 'Museo de Bellas Artes'),
(4, 'Aumento de un 15% de couta', 1, '2015-10-06 19:43:42', '2015-12-15 09:00:00', 0x41756d656e746f20646520756e2031352520646520636f757461, 6, 'Colegio');

-- --------------------------------------------------------

--
-- Table structure for table `eventosdestino`
--

DROP TABLE IF EXISTS `eventosdestino`;
CREATE TABLE `eventosdestino` (
  `eventos_id` int(11) NOT NULL,
  `destino_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eventosusers`
--

DROP TABLE IF EXISTS `eventosusers`;
CREATE TABLE `eventosusers` (
  `eventos_id` int(11) NOT NULL,
  `users_id` bigint(20) unsigned NOT NULL,
  `visto` int(11) DEFAULT NULL,
  `aceptado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `eventosusers`
--

INSERT INTO `eventosusers` (`eventos_id`, `users_id`, `visto`, `aceptado`) VALUES
(1, 3, 0, 0),
(1, 4, 0, 0),
(1, 5, 0, 0),
(1, 6, 0, 0),
(2, 3, 0, 0),
(2, 4, 1, 0),
(2, 5, 1, 1),
(2, 6, 0, 0),
(3, 4, 0, 0),
(4, 5, 1, 0),
(4, 6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrador'),
(2, 'members', 'Usuario por defecto de IonAuth'),
(3, 'editor', ''),
(4, 'developer', ''),
(5, 'super', ''),
(6, 'alumno', ''),
(7, 'preceptor', ''),
(8, 'departamentoalumnos', ''),
(9, 'departamentodocentes', ''),
(10, 'docente', ''),
(11, 'tutor', '');

-- --------------------------------------------------------

--
-- Table structure for table `horarios`
--

DROP TABLE IF EXISTS `horarios`;
CREATE TABLE `horarios` (
  `id` int(11) NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `horarios`
--

INSERT INTO `horarios` (`id`, `hora_inicio`, `hora_fin`) VALUES
(1, '07:30:00', '08:10:00'),
(2, '08:10:00', '08:50:00'),
(3, '09:00:00', '09:40:00'),
(4, '09:40:00', '10:20:00'),
(5, '10:30:00', '11:10:00'),
(6, '11:10:00', '11:50:00'),
(7, '12:00:00', '12:40:00'),
(8, '14:00:00', '14:40:00'),
(9, '14:45:00', '15:25:00'),
(10, '15:30:00', '16:10:00'),
(11, '16:20:00', '17:00:00'),
(12, '17:05:00', '17:45:00'),
(13, '17:50:00', '18:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `horariosmaterias`
--

DROP TABLE IF EXISTS `horariosmaterias`;
CREATE TABLE `horariosmaterias` (
  `horarios_id` int(11) NOT NULL,
  `materias_id` int(11) NOT NULL,
  `dias_id` int(11) NOT NULL,
  `divisiones_id` int(11) NOT NULL,
  `lugar` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `horariosmaterias`
--

INSERT INTO `horariosmaterias` (`horarios_id`, `materias_id`, `dias_id`, `divisiones_id`, `lugar`) VALUES
(1, 4, 5, 1, 'aula 1'),
(1, 6, 4, 1, 'aula 1'),
(1, 8, 3, 1, 'aula 1'),
(1, 9, 1, 2, 'aula 1'),
(1, 9, 2, 1, 'aula 1'),
(1, 9, 2, 2, 'aula 1'),
(1, 9, 3, 2, 'aula 1'),
(1, 9, 4, 2, 'aula 1'),
(1, 9, 5, 2, 'aula 1'),
(1, 12, 1, 1, 'aula 1'),
(1, 33, 1, 9, 'aula 1'),
(1, 33, 2, 9, 'aula 1'),
(1, 33, 3, 9, 'aula 1'),
(1, 33, 4, 9, 'aula 1'),
(1, 33, 5, 9, 'aula 1'),
(2, 2, 4, 1, 'aula 1'),
(2, 2, 5, 1, 'aula 1'),
(2, 3, 3, 1, 'aula 1'),
(2, 10, 1, 2, 'aula 1'),
(2, 10, 2, 2, 'aula 1'),
(2, 10, 3, 2, 'aula 1'),
(2, 10, 4, 2, 'aula 1'),
(2, 10, 5, 2, 'aula 1'),
(2, 13, 1, 1, 'aula 1'),
(2, 14, 2, 1, 'aula 1'),
(2, 34, 1, 9, 'aula 1'),
(2, 34, 2, 9, 'aula 1'),
(2, 34, 3, 9, 'aula 1'),
(2, 34, 4, 9, 'aula 1'),
(2, 34, 5, 9, 'aula 1'),
(3, 9, 1, 1, 'aula 1'),
(3, 10, 2, 1, 'aula 1'),
(3, 11, 1, 2, 'aula 1'),
(3, 11, 2, 2, 'aula 1'),
(3, 11, 3, 1, 'aula 1'),
(3, 11, 3, 2, 'aula 1'),
(3, 11, 4, 2, 'aula 1'),
(3, 11, 5, 2, 'aula 1'),
(3, 12, 4, 1, 'aula 1'),
(3, 13, 5, 1, 'aula 1'),
(3, 35, 1, 9, 'aula 1'),
(3, 35, 2, 9, 'aula 1'),
(3, 35, 3, 9, 'aula 1'),
(3, 35, 4, 9, 'aula 1'),
(3, 35, 5, 9, 'aula 1'),
(4, 1, 3, 1, 'aula 1'),
(4, 1, 4, 1, 'aula 1'),
(4, 5, 1, 1, 'aula 1'),
(4, 12, 1, 2, 'aula 1'),
(4, 12, 2, 2, 'aula 1'),
(4, 12, 3, 2, 'aula 1'),
(4, 12, 4, 2, 'aula 1'),
(4, 12, 5, 2, 'aula 1'),
(4, 15, 2, 1, 'aula 1'),
(4, 16, 5, 1, 'aula 1'),
(4, 36, 1, 9, 'aula 1'),
(4, 36, 2, 9, 'aula 1'),
(4, 36, 3, 9, 'aula 1'),
(4, 36, 4, 9, 'aula 1'),
(4, 36, 5, 9, 'aula 1'),
(5, 4, 4, 1, 'aula 1'),
(5, 6, 5, 1, 'aula 1'),
(5, 7, 3, 1, 'aula 1'),
(5, 8, 2, 1, 'aula 1'),
(5, 13, 1, 2, 'aula 1'),
(5, 13, 2, 2, 'aula 1'),
(5, 13, 3, 2, 'aula 1'),
(5, 13, 4, 2, 'aula 1'),
(5, 13, 5, 2, 'aula 1'),
(5, 14, 1, 1, 'aula 1'),
(5, 37, 1, 9, 'aula 1'),
(5, 37, 2, 9, 'aula 1'),
(5, 37, 3, 9, 'aula 1'),
(5, 37, 4, 9, 'aula 1'),
(5, 37, 5, 9, 'aula 1'),
(6, 3, 1, 1, 'aula 1'),
(6, 5, 4, 1, 'aula 1'),
(6, 7, 5, 1, 'aula 1'),
(6, 10, 3, 1, 'aula 1'),
(6, 11, 2, 1, 'aula 1'),
(6, 14, 1, 2, 'aula 1'),
(6, 14, 2, 2, 'aula 1'),
(6, 14, 3, 2, 'aula 1'),
(6, 14, 4, 2, 'aula 1'),
(6, 14, 5, 2, 'aula 1'),
(6, 38, 1, 9, 'aula 1'),
(6, 38, 2, 9, 'aula 1'),
(6, 38, 3, 9, 'aula 1'),
(6, 38, 4, 9, 'aula 1'),
(6, 38, 5, 9, 'aula 1');

-- --------------------------------------------------------

--
-- Table structure for table `inscripciones`
--

DROP TABLE IF EXISTS `inscripciones`;
CREATE TABLE `inscripciones` (
  `id` int(11) NOT NULL,
  `alumno_id` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL,
  `planesestudio_id` int(11) NOT NULL,
  `divisiones_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inscripciones`
--

INSERT INTO `inscripciones` (`id`, `alumno_id`, `activo`, `planesestudio_id`, `divisiones_id`) VALUES
(1, 3, 1, 4, 1),
(2, 4, 1, 4, 1),
(3, 5, 1, 4, 2),
(4, 6, 1, 13, 9);

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(2, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', 1, 1, 0, NULL, 1451403227);

-- --------------------------------------------------------

--
-- Table structure for table `limits`
--

DROP TABLE IF EXISTS `limits`;
CREATE TABLE `limits` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=659 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `rtime`, `authorized`, `response_code`) VALUES
(1, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '200.0.236.252', 1451480637, 0.016432, '1', 200),
(2, 'api/v1/tutor/tutor/17', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '200.0.236.252', 1451480649, 0.0278111, '1', 200),
(3, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452096960, 0.023917, '1', 200),
(4, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452097001, 0.017205, '1', 200),
(5, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452097256, 0.013397, '1', 200),
(6, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452101028, 0.0227418, '1', 200),
(7, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452106094, 0.0220208, '1', 200),
(8, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452106301, 0.0136218, '1', 200),
(9, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452106462, 0.013021, '1', 200),
(10, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452107449, 0.0213199, '1', 200),
(11, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452108651, 0.0267148, '1', 200),
(12, 'api/v1/tutor/tutor/17', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.3.7.109', 1452109096, 0.041028, '1', 200),
(13, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '200.0.236.252', 1452112498, 0.0237889, '1', 200),
(14, 'api/v1/tutor/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.130.91', 1452174049, 0.0457079, '1', 200),
(15, 'api/v1/tutor/tutor/16', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.130.91', 1452175658, 0.013504, '1', 200),
(16, 'api/v1/tutor/tutor/16', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.177.63', 1452186563, 0.0287459, '1', 200),
(17, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452780986, 0.0120189, '1', 200),
(18, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452781026, 0.0157769, '1', 200),
(19, 'api/v1/prueba/tutores', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452781122, 0.0176811, '1', 200),
(20, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452781928, 0.015492, '1', 200),
(21, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452782048, 0.0413539, '1', 200),
(22, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452782107, 0.0122979, '1', 200),
(23, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452782144, 0.029578, '1', 200),
(24, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452782259, 0.0167739, '1', 200),
(25, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452783327, 0.0607362, '1', 200),
(26, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452783390, 0.0115972, '1', 200),
(27, 'api/v1/prueba/getSchedulesByStudentDay', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452783493, 0.013166, '1', 200),
(28, 'api/v1/prueba/getSchedulesByStudentDay/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452783864, 0.0161619, '1', 200),
(29, 'api/v1/prueba/getSchedulesByStudentDay/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452783884, 0.0112872, '1', 200),
(30, 'api/v1/prueba/getSchedulesByStudentDay/1', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452783893, 0.0270231, '1', 200),
(31, 'api/v1/prueba/getSchedulesByStudentDay/1', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452783995, 0.014159, '1', 200),
(32, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452784599, 0.0149751, '1', 200),
(33, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452784623, 0.012198, '1', 200),
(34, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452784693, 0.0115201, '1', 200),
(35, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452784709, 0.0171509, '1', 200),
(36, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452784753, 0.011622, '1', 200),
(37, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452785291, 0.0310311, '1', 200),
(38, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452788188, 0.0185881, '1', 200),
(39, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452788222, 0.0133729, '1', 200),
(40, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452788275, 0.027746, '1', 200),
(41, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452788342, 0.0229878, '1', 200),
(42, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789508, 0.0130861, '1', 200),
(43, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789554, 0.014256, '1', 200),
(44, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789594, 0.013484, '1', 200),
(45, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789602, 0.0143239, '1', 200),
(46, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789685, 0.012361, '1', 200),
(47, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789740, 0.011457, '1', 200),
(48, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789751, 0.015285, '1', 200),
(49, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452789986, 0.012352, '1', 200),
(50, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452790025, 0.0110149, '1', 200),
(51, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452790126, 0.014086, '1', 200),
(52, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452790132, 0.012845, '1', 200),
(53, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452791488, 0.0147219, '1', 200),
(54, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452793977, 0.0322592, '1', 200),
(55, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452793988, 0.0410681, '1', 200),
(56, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794003, 0.0168118, '1', 200),
(57, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794024, 0.018692, '1', 200),
(58, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794088, 0.0132771, '1', 200),
(59, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794562, 0.0101321, '1', 200),
(60, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794579, 0.039722, '1', 200),
(61, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794595, 0.0147691, '1', 200),
(62, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794602, 0.0129781, '1', 200),
(63, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794609, 0.0117359, '1', 200),
(64, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794622, 0.0122001, '1', 200),
(65, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794626, 0.0113699, '1', 200),
(66, 'api/v1/prueba/getSchedulesByStudentDay/4/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794629, 0.016752, '1', 200),
(67, 'api/v1/prueba/getSchedulesByStudentDay/5/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794633, 0.0116432, '1', 200),
(68, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452794636, 0.0111911, '1', 200),
(69, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452795493, 0.0155079, '1', 200),
(70, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796108, 0.0397139, '1', 200),
(71, 'api/v1/prueba/getSchedulesByStudentDay/1/0', 'get', 'a:2:{i:0;s:1:"0";i:1;s:1:"0";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796108, 0.0273039, '1', 200),
(72, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796123, 0.0149651, '1', 200),
(73, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796126, 0.014708, '1', 200),
(74, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796138, 0.0140581, '1', 200),
(75, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796276, 0.0135572, '1', 200),
(76, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796282, 0.0127451, '1', 200),
(77, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796285, 0.0105062, '1', 200),
(78, 'api/v1/prueba/getSchedulesByStudentDay/1/4', 'get', 'a:2:{i:0;s:1:"4";i:1;s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796288, 0.0168228, '1', 200),
(79, 'api/v1/prueba/getSchedulesByStudentDay/2/4', 'get', 'a:2:{i:0;s:1:"4";i:1;s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796327, 0.0147648, '1', 200),
(80, 'api/v1/prueba/getSchedulesByStudentDay/3/4', 'get', 'a:2:{i:0;s:1:"4";i:1;s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796449, 0.0149279, '1', 200),
(81, 'api/v1/prueba/getSchedulesByStudentDay/3/5', 'get', 'a:2:{i:0;s:1:"5";i:1;s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796450, 0.0184948, '1', 200),
(82, 'api/v1/prueba/getSchedulesByStudentDay/3/6', 'get', 'a:2:{i:0;s:1:"6";i:1;s:1:"6";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796451, 0.012393, '1', 200),
(83, 'api/v1/prueba/getSchedulesByStudentDay/3/7', 'get', 'a:2:{i:0;s:1:"7";i:1;s:1:"7";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796452, 0.011745, '1', 200),
(84, 'api/v1/prueba/getSchedulesByStudentDay/3/8', 'get', 'a:2:{i:0;s:1:"8";i:1;s:1:"8";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796452, 0.0139511, '1', 200),
(85, 'api/v1/prueba/getSchedulesByStudentDay/3/9', 'get', 'a:2:{i:0;s:1:"9";i:1;s:1:"9";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796452, 0.011301, '1', 200),
(86, 'api/v1/prueba/getSchedulesByStudentDay/3/10', 'get', 'a:2:{i:0;s:2:"10";i:1;s:2:"10";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796452, 0.0105011, '1', 200),
(87, 'api/v1/prueba/getSchedulesByStudentDay/3/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796452, 0.014272, '1', 200),
(88, 'api/v1/prueba/getSchedulesByStudentDay/4/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796461, 0.0146589, '1', 200),
(89, 'api/v1/prueba/getSchedulesByStudentDay/5/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796461, 0.010731, '1', 200),
(90, 'api/v1/prueba/getSchedulesByStudentDay/1/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796461, 0.0116701, '1', 200),
(91, 'api/v1/prueba/getSchedulesByStudentDay/2/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796461, 0.0118139, '1', 200),
(92, 'api/v1/prueba/getSchedulesByStudentDay/3/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796461, 0.0121179, '1', 200),
(93, 'api/v1/prueba/getSchedulesByStudentDay/4/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796461, 0.0121529, '1', 200),
(94, 'api/v1/prueba/getSchedulesByStudentDay/5/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796462, 0.0125721, '1', 200),
(95, 'api/v1/prueba/getSchedulesByStudentDay/1/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796462, 0.015233, '1', 200),
(96, 'api/v1/prueba/getSchedulesByStudentDay/2/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796462, 0.016619, '1', 200),
(97, 'api/v1/prueba/getSchedulesByStudentDay/3/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796462, 0.0128889, '1', 200),
(98, 'api/v1/prueba/getSchedulesByStudentDay/4/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796462, 0.01262, '1', 200),
(99, 'api/v1/prueba/getSchedulesByStudentDay/5/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796463, 0.010124, '1', 200),
(100, 'api/v1/prueba/getSchedulesByStudentDay/1/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796463, 0.048948, '1', 200),
(101, 'api/v1/prueba/getSchedulesByStudentDay/2/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796463, 0.011795, '1', 200),
(102, 'api/v1/prueba/getSchedulesByStudentDay/3/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796463, 0.0135541, '1', 200),
(103, 'api/v1/prueba/getSchedulesByStudentDay/4/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796463, 0.0101759, '1', 200),
(104, 'api/v1/prueba/getSchedulesByStudentDay/5/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796463, 0.0135279, '1', 200),
(105, 'api/v1/prueba/getSchedulesByStudentDay/1/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796464, 0.0493782, '1', 200),
(106, 'api/v1/prueba/getSchedulesByStudentDay/2/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796464, 0.016094, '1', 200),
(107, 'api/v1/prueba/getSchedulesByStudentDay/3/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796464, 0.0113509, '1', 200),
(108, 'api/v1/prueba/getSchedulesByStudentDay/4/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796464, 0.011904, '1', 200),
(109, 'api/v1/prueba/getSchedulesByStudentDay/5/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796464, 0.012677, '1', 200),
(110, 'api/v1/prueba/getSchedulesByStudentDay/1/11', 'get', 'a:2:{i:0;s:2:"11";i:1;s:2:"11";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452796464, 0.0158751, '1', 200),
(111, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797468, 0.0165658, '1', 200),
(112, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797472, 0.011477, '1', 200),
(113, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797480, 0.0138521, '1', 200),
(114, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797488, 0.0121889, '1', 200),
(115, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797494, 0.0144389, '1', 200),
(116, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797496, 0.0136499, '1', 200),
(117, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797497, 0.012867, '1', 200),
(118, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797552, 0.045778, '1', 200),
(119, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797553, 0.0218799, '1', 200),
(120, 'api/v1/prueba/getSchedulesByStudentDay/4/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797555, 0.0133159, '1', 200),
(121, 'api/v1/prueba/getSchedulesByStudentDay/5/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797557, 0.0150042, '1', 200),
(122, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797558, 0.030046, '1', 200),
(123, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797559, 0.0125692, '1', 200),
(124, 'api/v1/prueba/getSchedulesByStudentDay/5/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797561, 0.011091, '1', 200),
(125, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797564, 0.0142381, '1', 200),
(126, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797565, 0.013298, '1', 200),
(127, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797567, 0.0149601, '1', 200),
(128, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797568, 0.016737, '1', 200),
(129, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797569, 0.0196569, '1', 200),
(130, 'api/v1/prueba/getSchedulesByStudentDay/4/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797570, 0.0139751, '1', 200),
(131, 'api/v1/prueba/getSchedulesByStudentDay/4/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797572, 0.014262, '1', 200),
(132, 'api/v1/prueba/getSchedulesByStudentDay/4/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797573, 0.0125432, '1', 200),
(133, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797574, 0.050993, '1', 200),
(134, 'api/v1/prueba/getSchedulesByStudentDay/4/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797575, 0.014405, '1', 200),
(135, 'api/v1/prueba/getSchedulesByStudentDay/4/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797576, 0.0148048, '1', 200),
(136, 'api/v1/prueba/getSchedulesByStudentDay/4/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797577, 0.0205841, '1', 200),
(137, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797679, 0.0173998, '1', 200),
(138, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797688, 0.0471909, '1', 200),
(139, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797690, 0.0128839, '1', 200),
(140, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797693, 0.0113392, '1', 200),
(141, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797697, 0.0150931, '1', 200),
(142, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797705, 0.0119359, '1', 200),
(143, 'api/v1/prueba/getSchedulesByStudentDay/4/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797710, 0.010709, '1', 200),
(144, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797719, 0.0121341, '1', 200),
(145, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797720, 0.0127912, '1', 200),
(146, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797721, 0.012615, '1', 200),
(147, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797723, 0.01618, '1', 200),
(148, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797724, 0.010699, '1', 200),
(149, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797725, 0.016366, '1', 200),
(150, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797726, 0.0123801, '1', 200),
(151, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797728, 0.011529, '1', 200),
(152, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797728, 0.0116482, '1', 200),
(153, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797729, 0.0508659, '1', 200),
(154, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797729, 0.0132561, '1', 200),
(155, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797730, 0.0529699, '1', 200),
(156, 'api/v1/prueba/getSchedulesByStudentDay/4/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797731, 0.0578301, '1', 200),
(157, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797732, 0.011744, '1', 200),
(158, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797734, 0.012712, '1', 200),
(159, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797735, 0.013196, '1', 200),
(160, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797737, 0.011054, '1', 200),
(161, 'api/v1/prueba/getSchedulesByStudentDay/4/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797745, 0.0140069, '1', 200),
(162, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797755, 0.012331, '1', 200),
(163, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797756, 0.0125098, '1', 200),
(164, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797757, 0.0117919, '1', 200),
(165, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1452797761, 0.0120239, '1', 200),
(166, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862214, 0.115716, '1', 200),
(167, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862304, 0.0115271, '1', 200),
(168, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862305, 0.0137389, '1', 200),
(169, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862308, 0.0120571, '1', 200),
(170, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862309, 0.0126979, '1', 200),
(171, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862316, 0.0212779, '1', 200),
(172, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862340, 0.0129678, '1', 200),
(173, 'api/v1/prueba/getSchedulesByStudentDay/4/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862342, 0.044091, '1', 200),
(174, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862351, 0.0112331, '1', 200),
(175, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862352, 0.0117941, '1', 200),
(176, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862573, 0.012023, '1', 200),
(177, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862573, 0.01142, '1', 200),
(178, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862575, 0.0119901, '1', 200),
(179, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862575, 0.046273, '1', 200),
(180, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452862576, 0.0100219, '1', 200),
(181, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.3.33', 1452863607, 0.0173969, '1', 200),
(182, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140077, 0.126596, '1', 200),
(183, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140081, 0.030973, '1', 200),
(184, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140083, 0.013665, '1', 200),
(185, 'api/v1/prueba/getSchedulesByStudentDay/4/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140084, 0.0150392, '1', 200),
(186, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140086, 0.0123651, '1', 200),
(187, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140087, 0.046319, '1', 200),
(188, 'api/v1/prueba/getSchedulesByStudentDay/5/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140088, 0.015594, '1', 200),
(189, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140091, 0.013104, '1', 200),
(190, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140092, 0.022275, '1', 200),
(191, 'api/v1/prueba/getSchedulesByStudentDay/5/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140093, 0.0110011, '1', 200),
(192, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140095, 0.0116789, '1', 200),
(193, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140095, 0.012692, '1', 200),
(194, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140095, 0.011102, '1', 200),
(195, 'api/v1/prueba/getSchedulesByStudentDay/4/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140096, 0.0115631, '1', 200),
(196, 'api/v1/prueba/getSchedulesByStudentDay/5/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140096, 0.014276, '1', 200),
(197, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140097, 0.0126221, '1', 200),
(198, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140097, 0.0162828, '1', 200),
(199, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453140098, 0.0111721, '1', 200),
(200, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141277, 0.0193951, '1', 200),
(201, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141379, 0.0124781, '1', 200),
(202, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141380, 0.0136502, '1', 200),
(203, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141381, 0.0120249, '1', 200),
(204, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141382, 0.0220931, '1', 200),
(205, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141383, 0.0566452, '1', 200),
(206, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141387, 0.054539, '1', 200),
(207, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141388, 0.0124152, '1', 200),
(208, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141389, 0.012008, '1', 200),
(209, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141396, 0.0136101, '1', 200),
(210, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141398, 0.0116699, '1', 200),
(211, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141400, 0.0137551, '1', 200),
(212, 'api/v1/prueba/getSchedulesByStudentDay/3/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141402, 0.0131452, '1', 200),
(213, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141403, 0.014045, '1', 200),
(214, 'api/v1/prueba/getSchedulesByStudentDay/4/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141406, 0.013988, '1', 200),
(215, 'api/v1/prueba/getSchedulesByStudentDay/4/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453141409, 0.0159128, '1', 200),
(216, 'api/v1/suscripcion/guardarEmail', 'post', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453147650, 0.0127039, '1', 0),
(217, 'api/v1/suscripcion/guardarEmail', 'post', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453147727, 0.012604, '1', 0),
(218, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:0:"";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453148014, 0.013068, '1', 0),
(219, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453148867, 0.152133, '1', 0),
(220, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149127, 0.010061, '1', 0),
(221, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149244, 0.0128701, '1', 0),
(222, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149273, 0.013824, '1', 0),
(223, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149308, 0.028806, '1', 0),
(224, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149339, 0.01126, '1', 0),
(225, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149378, 0.011014, '1', 0),
(226, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149550, 0.0122209, '1', 0),
(227, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149567, 0.0118401, '1', 0),
(228, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149604, 0.010006, '1', 0),
(229, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149627, NULL, '1', 0),
(230, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149653, 0.0119722, '1', 0),
(231, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian1@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149752, 0.011972, '1', 0),
(232, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:0:"";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149813, NULL, '1', 0),
(233, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:0:"";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453149902, NULL, '1', 0),
(234, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:0:"";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150062, 0.0152271, '1', 0),
(235, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:0:"";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150097, 0.014462, '1', 0),
(236, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150160, NULL, '1', 0),
(237, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150165, NULL, '1', 0),
(238, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150191, NULL, '1', 0),
(239, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150209, NULL, '1', 0),
(240, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150233, 0.0132289, '1', 0),
(241, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150292, NULL, '1', 0),
(242, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150372, 0.017415, '1', 0),
(243, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150414, 0.0113211, '1', 0),
(244, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150441, 0.0532298, '1', 0),
(245, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150504, 0.0132511, '1', 0),
(246, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453150553, 0.014935, '1', 0),
(247, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151147, 0.013392, '1', 0),
(248, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian1@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151158, 0.0123692, '1', 0),
(249, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian2@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151178, 0.010843, '1', 0),
(250, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian2@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151203, 0.0180759, '1', 0),
(251, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian2@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151283, 0.0139511, '1', 0),
(252, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian3@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151289, 0.010556, '1', 0),
(253, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151449, 0.014647, '1', 0),
(254, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151452, 0.0154898, '1', 0),
(255, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151543, 0.010406, '1', 0),
(256, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151554, 0.0138512, '1', 0),
(257, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151555, 0.0115731, '1', 0),
(258, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151556, 0.0104411, '1', 0),
(259, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151595, 0.011611, '1', 0),
(260, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"francisco@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151636, 0.011445, '1', 0),
(261, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"francisco@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151650, 0.00868702, '1', 0),
(262, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151715, 0.053721, '1', 0),
(263, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:20:"sebastian@e-nodo.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453151770, 0.011755, '1', 0),
(264, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:21:"sebastian@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453152114, 0.0159369, '1', 0),
(265, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:23:"sebastian10@e-nodos.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453152124, 0.01246, '1', 0),
(266, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '200.0.236.252', 1453154295, 0.303627, '1', 200),
(267, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '200.0.236.252', 1453154312, 0.0901451, '1', 200),
(268, 'api/v1/suscripcion/guardarEmail', 'post', 'a:1:{s:5:"email";s:22:"ecorona@bhp-global.com";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '66.249.88.233', 1453159455, 0.024791, '1', 0),
(269, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221923, 0.323326, '1', 200),
(270, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221936, 0.0187631, '1', 200),
(271, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221941, 0.013907, '1', 200),
(272, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221942, 0.0129561, '1', 200),
(273, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221944, 0.014667, '1', 200);
INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `rtime`, `authorized`, `response_code`) VALUES
(274, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221946, 0.0169508, '1', 200),
(275, 'api/v1/prueba/getSchedulesByStudentDay/4/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221950, 0.0102551, '1', 200),
(276, 'api/v1/prueba/getSchedulesByStudentDay/5/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221952, 0.0286481, '1', 200),
(277, 'api/v1/prueba/getSchedulesByStudentDay/5/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453221954, 0.036701, '1', 200),
(278, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228768, 0.103356, '1', 200),
(279, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228868, 0.0124741, '1', 200),
(280, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228871, 0.0129781, '1', 200),
(281, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228876, 0.0135181, '1', 200),
(282, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228879, 0.0239921, '1', 200),
(283, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228880, 0.011713, '1', 200),
(284, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228882, 0.01509, '1', 200),
(285, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228883, 0.0193021, '1', 200),
(286, 'api/v1/prueba/getSchedulesByStudentDay/4/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228885, 0.0129728, '1', 200),
(287, 'api/v1/prueba/getSchedulesByStudentDay/4/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228887, 0.0132799, '1', 200),
(288, 'api/v1/prueba/getSchedulesByStudentDay/5/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228889, 0.012116, '1', 200),
(289, 'api/v1/prueba/getSchedulesByStudentDay/4/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453228890, 0.0132241, '1', 200),
(290, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453229260, 0.046551, '1', 200),
(291, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453229312, 0.0152311, '1', 200),
(292, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453229397, 0.0127499, '1', 200),
(293, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.154.170', 1453229496, 0.0133131, '1', 200),
(294, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453231173, 0.042767, '1', 200),
(295, 'api/v1/prueba/getEvents/user/1234', 'get', 'a:1:{s:4:"user";s:4:"1234";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453231230, 0.049715, '1', 200),
(296, 'api/v1/prueba/getEvents/user/1234', 'get', 'a:1:{s:4:"user";s:4:"1234";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453231373, 0.0141089, '1', 200),
(297, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234453, 0.016151, '1', 200),
(298, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234460, 0.0129938, '1', 200),
(299, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234470, 0.0131969, '1', 200),
(300, 'api/v1/prueba/getSchedulesByStudentDay/3/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234479, 0.0140741, '1', 200),
(301, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234528, 0.0110919, '1', 200),
(302, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234876, 0.012259, '1', 200),
(303, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234885, 0.0140369, '1', 200),
(304, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234888, 0.012336, '1', 200),
(305, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234892, 0.0529909, '1', 200),
(306, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234895, 0.013711, '1', 200),
(307, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453234896, 0.0115671, '1', 200),
(308, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.181.94', 1453236667, 0.0680239, '1', 200),
(309, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.167.141', 1453250794, NULL, '1', 0),
(310, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.167.141', 1453250951, NULL, '1', 0),
(311, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.167.141', 1453250970, 0.0696449, '1', 200),
(312, 'api/v1/evento/eventos', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.167.141', 1453251547, 0.0141621, '1', 400),
(313, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.167.141', 1453251568, 0.050091, '1', 200),
(314, 'api/v1/evento/eventos', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.167.141', 1453251589, 0.0170071, '1', 200),
(315, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453297574, 0.0583808, '1', 200),
(316, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453301838, 0.0190761, '1', 200),
(317, 'api/v1/prueba/evento/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453302488, 0.0110619, '1', 200),
(318, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453303529, 0.0204968, '1', 200),
(319, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453303933, 0.0813458, '1', 200),
(320, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453304460, 0.0209351, '1', 200),
(321, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305188, 0.020498, '1', 200),
(322, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305284, 0.0680151, '1', 200),
(323, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305458, 0.017041, '1', 200),
(324, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305571, 0.019052, '1', 200),
(325, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305664, 0.0165989, '1', 200),
(326, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305730, 0.0134771, '1', 200),
(327, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305845, 0.0720229, '1', 200),
(328, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453305985, 0.0181122, '1', 200),
(329, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453310407, 0.020359, '1', 200),
(330, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453310529, 0.025846, '1', 200),
(331, 'api/v1/evento/obtener/null', 'get', 'a:1:{s:4:"null";N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453310534, 0.0319731, '1', 400),
(332, 'api/v1/evento/obtener/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453310655, 0.0124578, '1', 400),
(333, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453311218, 0.0685601, '1', 200),
(334, 'api/v1/evento/obtener/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453311226, 0.0141041, '1', 400),
(335, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453311328, 0.0159061, '1', 200),
(336, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453311447, 0.025209, '1', 400),
(337, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453311810, 0.0124321, '1', 400),
(338, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453311855, 0.0128539, '1', 400),
(339, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453312269, 0.028502, '1', 200),
(340, 'api/v1/evento/obtener/3', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453312281, 0.0415509, '1', 200),
(341, 'api/v1/evento/obtener/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453312606, 0.019294, '1', 200),
(342, 'api/v1/evento/obtener/1', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453312779, 0.01829, '1', 200),
(343, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453314054, 0.0202351, '1', 200),
(344, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453314197, 0.0121739, '1', 200),
(345, 'api/v1/evento/obtener/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453314223, 0.018054, '1', 200),
(346, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453314444, 0.0153251, '1', 200),
(347, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453314448, 0.0586591, '1', 200),
(348, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453315175, 0.016417, '1', 200),
(349, 'api/v1/evento/obtener/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453315182, 0.0198362, '1', 200),
(350, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453315589, 0.063426, '1', 200),
(351, 'api/v1/evento/obtener/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453315595, 0.0637789, '1', 200),
(352, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453316115, 0.061944, '1', 200),
(353, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453316117, 0.0496969, '1', 200),
(354, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453316135, 0.018429, '1', 200),
(355, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453316180, 0.0124161, '1', 200),
(356, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453316334, 0.0153241, '1', 200),
(357, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453316819, 0.021126, '1', 200),
(358, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453316824, 0.0165639, '1', 200),
(359, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453317238, 0.0559428, '1', 200),
(360, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453317242, 0.0145211, '1', 200),
(361, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453318135, 0.013948, '1', 200),
(362, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453318145, 0.0258889, '1', 200),
(363, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453319945, 0.018851, '1', 200),
(364, 'api/v1/evento/obtener/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453319949, 0.0231869, '1', 200),
(365, 'api/v1/evento/obtener/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453321095, 0.022012, '1', 200),
(366, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453321098, 0.0123758, '1', 200),
(367, 'api/v1/evento/obtener/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453321102, 0.015528, '1', 200),
(368, 'api/v1/evento/obtener/id/3', 'get', 'a:1:{s:2:"id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453321115, 0.025528, '1', 200),
(369, 'api/v1/evento/obtener/id/3', 'get', 'a:1:{s:2:"id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453321309, 0.0170791, '1', 200),
(370, 'api/v1/evento/obtener/id/3', 'get', 'a:1:{s:2:"id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453321358, 0.015748, '1', 200),
(371, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.138.172', 1453327304, 0.0184691, '1', 200),
(372, 'api/v1/evento/obtener/id/2', 'get', 'a:1:{s:2:"id";s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.138.172', 1453327318, 0.0191751, '1', 200),
(373, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.138.172', 1453327341, 0.053179, '1', 200),
(374, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.138.172', 1453327343, 0.0121748, '1', 200),
(375, 'api/v1/prueba/getSchedulesByStudentDay/2/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.138.172', 1453327345, 0.0123799, '1', 200),
(376, 'api/v1/prueba/getSchedulesByStudentDay/3/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.138.172', 1453327347, 0.0114579, '1', 200),
(377, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453383944, 0.153041, '1', 200),
(378, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453391819, 0.0971541, '1', 200),
(379, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453392062, 0.019855, '1', 200),
(380, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453392430, 0.015729, '1', 200),
(381, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453392739, 0.0236001, '1', 200),
(382, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453393029, 0.0275922, '1', 200),
(383, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453397724, 0.0300939, '1', 200),
(384, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453397930, 0.0244229, '1', 200),
(385, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453398055, 0.0216539, '1', 200),
(386, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453398125, 0.019057, '1', 200),
(387, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453468912, 0.0482919, '1', 200),
(388, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453469491, 0.0145471, '1', 200),
(389, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453469494, 0.025588, '1', 200),
(390, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453473336, 0.0156889, '1', 200),
(391, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453473347, 0.012794, '1', 200),
(392, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453473375, 0.016484, '1', 200),
(393, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453473381, 0.0123742, '1', 200),
(394, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453475692, 0.0230541, '1', 200),
(395, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453475697, 0.012321, '1', 200),
(396, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489427, 0.175332, '1', 200),
(397, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489431, 0.0143549, '1', 200),
(398, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489797, 0.01702, '1', 200),
(399, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489801, 0.0226099, '1', 200),
(400, 'api/v1/prueba/getSchedulesById/1', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489806, 0.050251, '1', 200),
(401, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489982, 0.033587, '1', 200),
(402, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489985, 0.0124381, '1', 200),
(403, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453489988, 0.025631, '1', 200),
(404, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490253, 0.0218031, '1', 200),
(405, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490256, 0.013984, '1', 200),
(406, 'api/v1/prueba/getSchedulesById/1', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490266, 0.011472, '1', 200),
(407, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490607, 0.0224578, '1', 200),
(408, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490615, 0.0161269, '1', 200),
(409, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490623, 0.043334, '1', 200),
(410, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490744, 0.016984, '1', 200),
(411, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490745, 0.0108321, '1', 200),
(412, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490747, 0.0101321, '1', 200),
(413, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490861, 0.0202391, '1', 200),
(414, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490878, 0.0132451, '1', 200),
(415, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453490882, 0.012711, '1', 200),
(416, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491058, 0.037426, '1', 200),
(417, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491062, 0.014087, '1', 200),
(418, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491064, 0.014576, '1', 200),
(419, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491211, 0.025965, '1', 200),
(420, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491215, 0.0152941, '1', 200),
(421, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491218, 0.015063, '1', 200),
(422, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491816, 0.0760691, '1', 200),
(423, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491835, 0.012291, '1', 200),
(424, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453491837, 0.0127611, '1', 200),
(425, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453492087, 0.0112441, '1', 200),
(426, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453492089, 0.0324361, '1', 200),
(427, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453492092, 0.0111609, '1', 200),
(428, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453493895, 0.015521, '1', 200),
(429, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453494010, 0.018132, '1', 200),
(430, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453494012, 0.0146029, '1', 200),
(431, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453494014, 0.013669, '1', 200),
(432, 'api/v1/prueba/getSchedulesById/1', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453494022, 0.0120101, '1', 200),
(433, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453494029, 0.0133669, '1', 200),
(434, 'api/v1/prueba/getSchedulesById/1', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453494030, 0.012856, '1', 200),
(435, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453494035, 0.0126019, '1', 200),
(436, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '181.117.3.123', 1453566113, 0.117241, '1', 200),
(437, 'api/v1/evento/obtener/id/3', 'get', 'a:1:{s:2:"id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '181.117.3.123', 1453566116, 0.0355999, '1', 200),
(438, 'api/v1/evento/obtener/id/3', 'get', 'a:1:{s:2:"id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '181.117.3.123', 1453566119, 0.0144281, '1', 200),
(439, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '181.117.3.123', 1453566123, 0.0122402, '1', 200),
(440, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.5.175', 1453667434, 0.116557, '1', 200),
(441, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.13.5.175', 1453667456, 0.0137761, '1', 200),
(442, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453820074, 0.298537, '1', 200),
(443, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453821208, 0.0311351, '1', 200),
(444, 'api/v1/prueba/getEvents', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453920433, 0.0754011, '1', 200),
(445, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453920451, 0.0132689, '1', 200),
(446, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453921584, 0.0205009, '1', 400),
(447, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453921656, 0.111631, '1', 0),
(448, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453922117, 0.0193369, '1', 200),
(449, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453922149, 0.0305219, '1', 400),
(450, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453922211, 0.0248358, '1', 400),
(451, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.39.153.202', 1453922378, 0.0916588, '1', 200),
(452, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.136.21', 1453930106, 0.0664561, '1', 200),
(453, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.136.21', 1453930126, 0.0115809, '1', 200),
(454, 'api/v1/evento/obtener/id/1', 'get', 'a:1:{s:2:"id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.136.21', 1453930132, 0.0295751, '1', 200),
(455, 'api/v1/evento/obtener/id/0', 'get', 'a:1:{s:2:"id";s:1:"0";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '190.179.136.21', 1453930161, 0.0550592, '1', 404),
(456, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.158.142.118', 1453941231, 0.031672, '1', 200),
(457, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.158.142.118', 1453941231, 0.0131261, '1', 200),
(458, 'api/v1/prueba/getSchedulesByStudentDay/1/2', 'get', 'a:2:{i:0;s:1:"2";i:1;s:1:"2";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.158.142.118', 1453941270, 0.0135169, '1', 200),
(459, 'api/v1/prueba/getSchedulesByStudentDay/1/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.158.142.118', 1453941272, 0.0456531, '1', 200),
(460, 'api/v1/prueba/getSchedulesByStudentDay/2/3', 'get', 'a:2:{i:0;s:1:"3";i:1;s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.158.142.118', 1453941397, 0.0168462, '1', 200),
(461, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453988249, 0.446457, '1', 200),
(462, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453988253, 0.0346961, '1', 200),
(463, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453988412, 0.0756371, '1', 200),
(464, 'api/v1/horario/obtener/dia/1/alumno_id/1', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453988417, 0.04337, '1', 400),
(465, 'api/v1/horario/obtener/dia/1/alumno_id/1', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453988491, 0.164655, '1', 0),
(466, 'api/v1/horario/obtener/dia/1/alumno_id/1', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453988538, 0.0184929, '1', 0),
(467, 'api/v1/horario/obtener/dia/1/alumno_id/1', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453988593, 0.0162408, '1', 0),
(468, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453990519, 0.032773, '1', 200),
(469, 'api/v1/horario/obtener/dia/1/alumno_id/1', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453990526, 0.0173979, '1', 400),
(470, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453990633, 0.028604, '1', 200),
(471, 'api/v1/horario/obtener/dia/1/alumno_id/1', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453990637, 0.012893, '1', 400),
(472, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453990967, 0.029058, '1', 200),
(473, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453990970, 0.0108099, '1', 400),
(474, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991006, 0.0215859, '1', 200),
(475, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991151, 0.018399, '1', 200),
(476, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991154, 0.014951, '1', 400),
(477, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991203, 0.017977, '1', 200),
(478, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991278, 0.027127, '1', 200),
(479, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991283, 0.0146401, '1', 400),
(480, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991774, 0.029629, '1', 200),
(481, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991781, 0.0135748, '1', 400),
(482, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991959, 0.021939, '1', 200),
(483, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991963, 0.021215, '1', 200),
(484, 'api/v1/prueba/getSchedulesById/2', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991980, 0.011194, '1', 200),
(485, 'api/v1/prueba/getSchedulesById/5', 'get', 'a:2:{i:0;N;i:1;N;}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453991996, 0.052705, '1', 200),
(486, 'api/v1/prueba/getSchedulesByStudentDay/2/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453992033, 0.0113118, '1', 200),
(487, 'api/v1/prueba/getSchedulesByStudentDay/1/1', 'get', 'a:2:{i:0;s:1:"1";i:1;s:1:"1";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453992036, 0.011255, '1', 200),
(488, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453993670, 0.0328391, '1', 200),
(489, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453993676, 0.0260952, '1', 200),
(490, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453993680, 0.018245, '1', 400),
(491, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453993716, 0.0257211, '1', 200),
(492, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453993963, 0.077116, '1', 200),
(493, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453993967, 0.020673, '1', 200),
(494, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453993978, 0.019202, '1', 200),
(495, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994194, 0.023716, '1', 200),
(496, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994198, 0.017169, '1', 200),
(497, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994200, 0.016273, '1', 200),
(498, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994206, 0.0212231, '1', 200),
(499, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994210, 0.0193009, '1', 200),
(500, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994213, 0.016104, '1', 200),
(501, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994216, 0.0172479, '1', 200),
(502, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994218, 0.0173759, '1', 200),
(503, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994219, 0.0162089, '1', 200),
(504, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994219, 0.025578, '1', 200),
(505, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994219, 0.016732, '1', 200),
(506, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994219, 0.0188601, '1', 200),
(507, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994220, 0.0255718, '1', 200),
(508, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994220, 0.019026, '1', 200),
(509, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994220, 0.0180161, '1', 200),
(510, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994220, 0.0196788, '1', 200),
(511, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994220, 0.0171971, '1', 200),
(512, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994220, 0.0177879, '1', 200),
(513, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994221, 0.075433, '1', 200),
(514, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994221, 0.0304079, '1', 200),
(515, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994221, 0.0200131, '1', 200),
(516, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994221, 0.019285, '1', 200),
(517, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994221, 0.026432, '1', 200),
(518, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994221, 0.0177999, '1', 200),
(519, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994223, 0.0174539, '1', 200),
(520, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994223, 0.0171871, '1', 200),
(521, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994223, 0.016058, '1', 200),
(522, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994223, 0.0570631, '1', 200),
(523, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994224, 0.02335, '1', 200),
(524, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994224, 0.0181451, '1', 200),
(525, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994224, 0.0165579, '1', 200),
(526, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994224, 0.018363, '1', 200),
(527, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994224, 0.0260611, '1', 200),
(528, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994225, 0.0161359, '1', 200),
(529, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994225, 0.0178199, '1', 200),
(530, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994225, 0.017174, '1', 200),
(531, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994225, 0.023011, '1', 200),
(532, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994225, 0.0176311, '1', 200),
(533, 'api/v1/horario/obtener/dia/2/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453994226, 0.018342, '1', 200),
(534, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453999940, 0.0260642, '1', 200),
(535, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453999943, 0.02175, '1', 200),
(536, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453999945, 0.0154579, '1', 200),
(537, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1453999974, 0.0210738, '1', 200),
(538, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454000088, 0.0198939, '1', 200),
(539, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454000089, 0.0190392, '1', 200),
(540, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454000090, 0.028697, '1', 200),
(541, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001066, 0.0505509, '1', 200),
(542, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001068, 0.0255311, '1', 200),
(543, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001072, 0.0172639, '1', 200),
(544, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001084, 0.0214789, '1', 200),
(545, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001140, 0.0495369, '1', 200),
(546, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001488, 0.0515001, '1', 200),
(547, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001491, 0.037431, '1', 200),
(548, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001493, 0.018625, '1', 200),
(549, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001494, 0.0212541, '1', 200),
(550, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001494, 0.015945, '1', 200),
(551, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001497, 0.0200591, '1', 200),
(552, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001498, 0.0158129, '1', 200),
(553, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001499, 0.017128, '1', 200),
(554, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001500, 0.023052, '1', 200),
(555, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001501, 0.018074, '1', 200),
(556, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001502, 0.0200238, '1', 200),
(557, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001503, 0.0217612, '1', 200),
(558, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454001578, 0.013263, '1', 0),
(559, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003675, 0.0185771, '1', 0),
(560, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003679, 0.0631371, '1', 0),
(561, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003679, 0.0208492, '1', 0),
(562, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003680, 0.0516171, '1', 0);
INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `rtime`, `authorized`, `response_code`) VALUES
(563, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003680, 0.0125549, '1', 0),
(564, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003680, 0.02367, '1', 0),
(565, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003680, 0.012785, '1', 0),
(566, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003681, 0.0162878, '1', 0),
(567, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003681, 0.01615, '1', 0),
(568, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003681, 0.0172141, '1', 0),
(569, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003682, 0.026432, '1', 0),
(570, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003682, 0.0156069, '1', 0),
(571, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003694, 0.01965, '1', 200),
(572, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454003696, 0.0283558, '1', 0),
(573, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004188, 0.0295119, '1', 200),
(574, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004191, 0.018574, '1', 200),
(575, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004193, 0.0190182, '1', 200),
(576, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004194, 0.0717669, '1', 200),
(577, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004196, 0.0382018, '1', 200),
(578, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004197, 0.0162499, '1', 200),
(579, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004315, 0.018415, '1', 404),
(580, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004319, 0.0179732, '1', 200),
(581, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004320, 0.020607, '1', 404),
(582, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004327, 0.018456, '1', 200),
(583, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004531, 0.0259449, '1', 200),
(584, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004534, 0.0171402, '1', 200),
(585, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004537, 0.0239961, '1', 200),
(586, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004540, 0.0561719, '1', 200),
(587, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004544, 0.020606, '1', 200),
(588, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004709, 0.0276299, '1', 200),
(589, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004717, 0.016856, '1', 200),
(590, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004947, 0.0297389, '1', 200),
(591, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454004948, 0.0508881, '1', 200),
(592, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005236, 0.0191, '1', 200),
(593, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005239, 0.0225248, '1', 200),
(594, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005240, 0.017401, '1', 200),
(595, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005421, 0.021009, '1', 200),
(596, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005423, 0.065773, '1', 200),
(597, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005425, 0.943795, '1', 200),
(598, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005428, 0.45751, '1', 200),
(599, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005431, 0.0193801, '1', 200),
(600, 'api/v1/horario/obtener/dia/5/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005434, 0.01811, '1', 200),
(601, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005436, 0.019717, '1', 200),
(602, 'api/v1/horario/obtener/dia/5/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005437, 0.0220881, '1', 200),
(603, 'api/v1/horario/obtener/dia/5/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005438, 0.0406601, '1', 200),
(604, 'api/v1/horario/obtener/dia/5/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005439, 0.0209038, '1', 200),
(605, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005439, 0.017839, '1', 200),
(606, 'api/v1/horario/obtener/dia/5/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005440, 0.015835, '1', 200),
(607, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005441, 0.0187361, '1', 200),
(608, 'api/v1/horario/obtener/dia/5/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005441, 0.0191731, '1', 200),
(609, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005442, 0.0331788, '1', 200),
(610, 'api/v1/horario/obtener/dia/5/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005442, 0.0244212, '1', 200),
(611, 'api/v1/horario/obtener/dia/5/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005442, 0.01842, '1', 200),
(612, 'api/v1/horario/obtener/dia/5/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005443, 0.041903, '1', 200),
(613, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005444, 0.016253, '1', 200),
(614, 'api/v1/horario/obtener/dia/5/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005445, 0.01825, '1', 200),
(615, 'api/v1/horario/obtener/dia/5/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005451, 0.017401, '1', 200),
(616, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005453, 0.022928, '1', 200),
(617, 'api/v1/horario/obtener/dia/5/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005454, 0.0176659, '1', 200),
(618, 'api/v1/horario/obtener/dia/5/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005462, 0.0200269, '1', 200),
(619, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005463, 0.0275419, '1', 200),
(620, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005465, 0.0179899, '1', 200),
(621, 'api/v1/horario/obtener/dia/1/alumno_id/4', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"4";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005466, 0.0175519, '1', 200),
(622, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005593, 0.0254691, '1', 200),
(623, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005598, 0.0464001, '1', 200),
(624, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005608, 0.017616, '1', 200),
(625, 'api/v1/horario/obtener/dia/2/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005611, 0.0196872, '1', 200),
(626, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005614, 0.0181799, '1', 200),
(627, 'api/v1/horario/obtener/dia/2/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005614, 0.0373781, '1', 200),
(628, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005615, 0.0157619, '1', 200),
(629, 'api/v1/horario/obtener/dia/2/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005616, 0.017355, '1', 200),
(630, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005616, 0.0169451, '1', 200),
(631, 'api/v1/horario/obtener/dia/2/alumno_id/5', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"5";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005617, 0.0197201, '1', 200),
(632, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005617, 0.0172532, '1', 200),
(633, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005618, 0.024539, '1', 200),
(634, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005619, 0.0174699, '1', 200),
(635, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005619, 0.016494, '1', 200),
(636, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005620, 0.0279291, '1', 200),
(637, 'api/v1/horario/obtener/dia/2/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"2";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005620, 0.029881, '1', 200),
(638, 'api/v1/horario/obtener/dia/3/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"3";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005621, 0.0183151, '1', 200),
(639, 'api/v1/horario/obtener/dia/4/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"4";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005621, 0.0176029, '1', 200),
(640, 'api/v1/horario/obtener/dia/5/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"5";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005622, 0.0176289, '1', 200),
(641, 'api/v1/horario/obtener/dia/1/alumno_id/3', 'get', 'a:2:{s:3:"dia";s:1:"1";s:9:"alumno_id";s:1:"3";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '186.159.120.238', 1454005622, 0.0201318, '1', 200),
(642, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454009112, 1.08999, '1', 400),
(643, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454009152, 1.16212, '1', 200),
(644, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454009186, 1.09459, '1', 200),
(645, 'api/v1/evento/obtener', 'get', NULL, '', '::1', 1454009451, 1.07183, '0', 403),
(646, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454009753, 1.07726, '1', 400),
(647, 'api/v1/login/acceder', 'post', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010308, 1.09398, '1', 400),
(648, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010447, 1.13089, '1', 200),
(649, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010713, 1.09381, '1', 0),
(650, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010740, 1.06524, '1', 0),
(651, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010819, 1.08188, '1', 200),
(652, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010871, 1.19504, '1', 200),
(653, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"epjpowje";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010911, 1.09365, '1', 404),
(654, 'api/v1/login/acceder', 'post', 'a:2:{s:5:"email";s:15:"admin@admin.com";s:8:"password";s:8:"password";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010942, 1.16324, '1', 200),
(655, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010975, 1.09912, '1', 200),
(656, 'api/v1/evento/obtener', 'get', NULL, 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454010991, 1.0924, '1', 200),
(657, 'api/v1/login/registro', 'post', 'a:5:{s:8:"username";s:7:"alfredo";s:8:"password";s:6:"lalala";s:5:"email";s:17:"alfredo@yahoo.com";s:6:"nombre";s:15:"Alfredo Joaquin";s:8:"apellido";s:5:"Gomez";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454012680, 1.08916, '1', 400),
(658, 'api/v1/login/registro', 'post', 'a:5:{s:8:"username";s:7:"alfredo";s:8:"password";s:6:"lalala";s:5:"email";s:17:"alfredo@yahoo.com";s:6:"nombre";s:15:"Alfredo Joaquin";s:8:"apellido";s:5:"Gomez";}', 'k8kokcg0swkk8kk4c0ogck40sgk88gcwwokwss0c', '::1', 1454012763, 1.15903, '1', 200);

-- --------------------------------------------------------

--
-- Table structure for table `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `id` int(11) NOT NULL,
  `tipocalificacion_id` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `horas` int(11) NOT NULL,
  `materia_id` int(11) NOT NULL,
  `materia_especial` int(11) NOT NULL,
  `tipo_acta` int(11) NOT NULL,
  `planesestudio_id` int(11) NOT NULL,
  `anios_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materias`
--

INSERT INTO `materias` (`id`, `tipocalificacion_id`, `orden`, `horas`, `materia_id`, `materia_especial`, `tipo_acta`, `planesestudio_id`, `anios_id`) VALUES
(1, 1, 1, 0, 0, 0, 0, 4, 4),
(2, 1, 2, 0, 0, 0, 0, 4, 4),
(3, 1, 3, 0, 0, 0, 0, 4, 4),
(4, 1, 4, 0, 0, 0, 0, 4, 4),
(5, 1, 5, 0, 0, 0, 0, 4, 4),
(6, 1, 6, 0, 0, 0, 0, 4, 4),
(7, 1, 7, 0, 0, 0, 0, 4, 4),
(8, 1, 8, 0, 0, 0, 0, 4, 4),
(9, 1, 1, 0, 0, 0, 0, 5, 5),
(10, 1, 2, 0, 0, 0, 0, 5, 5),
(11, 1, 3, 0, 0, 0, 0, 5, 5),
(12, 1, 4, 0, 0, 0, 0, 5, 5),
(13, 1, 5, 0, 0, 0, 0, 5, 5),
(14, 1, 6, 0, 0, 0, 0, 5, 5),
(15, 1, 7, 0, 0, 0, 0, 5, 5),
(16, 1, 8, 0, 0, 0, 0, 5, 5),
(17, 1, 1, 0, 0, 0, 0, 10, 6),
(18, 1, 2, 0, 0, 0, 0, 10, 6),
(19, 1, 3, 0, 0, 0, 0, 10, 6),
(20, 1, 4, 0, 0, 0, 0, 10, 6),
(21, 1, 5, 0, 0, 0, 0, 10, 6),
(22, 1, 6, 0, 0, 0, 0, 10, 6),
(23, 1, 7, 0, 0, 0, 0, 10, 6),
(24, 1, 8, 0, 0, 0, 0, 10, 6),
(25, 1, 1, 0, 0, 0, 0, 11, 7),
(26, 1, 2, 0, 0, 0, 0, 11, 7),
(27, 1, 3, 0, 0, 0, 0, 11, 7),
(28, 1, 4, 0, 0, 0, 0, 11, 7),
(29, 1, 5, 0, 0, 0, 0, 11, 7),
(30, 1, 6, 0, 0, 0, 0, 11, 7),
(31, 1, 7, 0, 0, 0, 0, 11, 7),
(32, 1, 8, 0, 0, 0, 0, 11, 7),
(33, 1, 1, 0, 0, 0, 0, 13, 8),
(34, 1, 2, 0, 0, 0, 0, 13, 8),
(35, 1, 3, 0, 0, 0, 0, 13, 8),
(36, 1, 4, 0, 0, 0, 0, 13, 8),
(37, 1, 5, 0, 0, 0, 0, 13, 8),
(38, 1, 6, 0, 0, 0, 0, 13, 8),
(39, 1, 7, 0, 0, 0, 0, 13, 8),
(40, 1, 8, 0, 0, 0, 0, 13, 8),
(41, 1, 1, 0, 0, 0, 0, 14, 14),
(42, 1, 2, 0, 0, 0, 0, 14, 14),
(43, 1, 3, 0, 0, 0, 0, 14, 14),
(44, 1, 4, 0, 0, 0, 0, 14, 14),
(45, 1, 5, 0, 0, 0, 0, 14, 14),
(46, 1, 6, 0, 0, 0, 0, 14, 14),
(47, 1, 7, 0, 0, 0, 0, 14, 14),
(48, 1, 8, 0, 0, 0, 0, 14, 14);

-- --------------------------------------------------------

--
-- Table structure for table `niveles`
--

DROP TABLE IF EXISTS `niveles`;
CREATE TABLE `niveles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `especialidad_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `niveles`
--

INSERT INTO `niveles` (`id`, `nombre`, `especialidad_id`) VALUES
(1, 'Educación Inicial', 0),
(2, 'Educación Primaria', 0),
(3, 'EDUCACION SECUNDARIA CICLO BASICO', 0),
(4, 'EDUCACION SECUNDARIA CICLO ORIENTADO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nodocolegio`
--

DROP TABLE IF EXISTS `nodocolegio`;
CREATE TABLE `nodocolegio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `codigo` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nodocolegio`
--

INSERT INTO `nodocolegio` (`id`, `nombre`, `codigo`) VALUES
(1, 'Colegio San Pablo', 'b4884303bbd79e9d03323a7ca49c02a8f3f0575d');

-- --------------------------------------------------------

--
-- Table structure for table `nododivision`
--

DROP TABLE IF EXISTS `nododivision`;
CREATE TABLE `nododivision` (
  `id` int(11) NOT NULL,
  `nodoColegio_id` int(11) NOT NULL,
  `planesestudio_id` int(11) NOT NULL,
  `divisiones_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nododivision`
--

INSERT INTO `nododivision` (`id`, `nodoColegio_id`, `planesestudio_id`, `divisiones_id`) VALUES
(1, 1, 4, 1),
(2, 1, 4, 2),
(3, 1, 5, 3),
(4, 1, 5, 4),
(5, 1, 10, 5),
(6, 1, 10, 6),
(7, 1, 11, 7),
(8, 1, 11, 8),
(9, 1, 13, 9),
(10, 1, 14, 10);

-- --------------------------------------------------------

--
-- Table structure for table `nombresmateria`
--

DROP TABLE IF EXISTS `nombresmateria`;
CREATE TABLE `nombresmateria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `materias_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nombresmateria`
--

INSERT INTO `nombresmateria` (`id`, `nombre`, `fecha`, `materias_id`) VALUES
(1, 'Matemática', '2015-12-11 09:53:10', 1),
(2, 'Lengua', '2015-12-11 09:53:10', 2),
(3, 'Geografía', '2015-12-11 09:53:10', 3),
(4, 'Historia', '2015-12-11 09:53:10', 4),
(5, 'Formación Ética y Ciudadana', '2015-12-11 09:53:10', 5),
(6, 'Música', '2015-12-11 09:53:10', 6),
(7, 'Educación Física', '2015-12-11 09:53:10', 7),
(8, 'Tecnología', '2015-12-11 09:53:10', 8),
(9, 'Matemática', '2015-12-11 09:53:10', 9),
(10, 'Lengua', '2015-12-11 09:53:10', 10),
(11, 'Geografía', '2015-12-11 09:53:10', 11),
(12, 'Historia', '2015-12-11 09:53:10', 12),
(13, 'Formación Ética y Ciudadana', '2015-12-11 09:53:10', 13),
(14, 'Música', '2015-12-11 09:53:10', 14),
(15, 'Educación Física', '2015-12-11 09:53:10', 15),
(16, 'Tecnología', '2015-12-11 09:53:10', 16),
(17, 'Matemática', '2015-12-11 09:53:10', 17),
(18, 'Lengua', '2015-12-11 09:53:10', 18),
(19, 'Geografía', '2015-12-11 09:53:10', 19),
(20, 'Historia', '2015-12-11 09:53:10', 20),
(21, 'Formación Ética y Ciudadana', '2015-12-11 09:53:10', 21),
(22, 'Música', '2015-12-11 09:53:10', 22),
(23, 'Educación Física', '2015-12-11 09:53:10', 23),
(24, 'Tecnología', '2015-12-11 09:53:10', 24),
(25, 'Matemática', '2015-12-11 09:53:10', 25),
(26, 'Lengua', '2015-12-11 09:53:10', 26),
(27, 'Geografía', '2015-12-11 09:53:10', 27),
(28, 'Historia', '2015-12-11 09:53:10', 28),
(29, 'Formación Ética y Ciudadana', '2015-12-11 09:53:10', 29),
(30, 'Música', '2015-12-11 09:53:10', 30),
(31, 'Educación Física', '2015-12-11 09:53:10', 31),
(32, 'Tecnología', '2015-12-11 09:53:10', 32),
(33, 'Matemática', '2015-12-11 09:53:10', 33),
(34, 'Lengua', '2015-12-11 09:53:10', 34),
(35, 'Geografía', '2015-12-11 09:53:10', 35),
(36, 'Historia', '2015-12-11 09:53:10', 36),
(37, 'Formación Ética y Ciudadana', '2015-12-11 09:53:10', 37),
(38, 'Música', '2015-12-11 09:53:10', 38),
(39, 'Educación Física', '2015-12-11 09:53:10', 39),
(40, 'Tecnología', '2015-12-11 09:53:10', 40),
(41, 'Matemática', '2015-12-11 09:53:10', 41),
(42, 'Lengua', '2015-12-11 09:53:10', 42),
(43, 'Geografía', '2015-12-11 09:53:10', 43),
(44, 'Historia', '2015-12-11 09:53:10', 44),
(45, 'Formación Ética y Ciudadana', '2015-12-11 09:53:10', 45),
(46, 'Música', '2015-12-11 09:53:10', 46),
(47, 'Educación Física', '2015-12-11 09:53:10', 47),
(48, 'Tecnología', '2015-12-11 09:53:10', 48);

-- --------------------------------------------------------

--
-- Table structure for table `periodosevaluacion`
--

DROP TABLE IF EXISTS `periodosevaluacion`;
CREATE TABLE `periodosevaluacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `inicio` date NOT NULL,
  `cierre` date NOT NULL,
  `secursa` tinyint(4) NOT NULL,
  `recuperacion` tinyint(4) NOT NULL,
  `orden` int(11) NOT NULL,
  `periodo_rel` int(11) NOT NULL,
  `bloquea_carga` tinyint(4) DEFAULT NULL,
  `id_turno` int(11) NOT NULL,
  `planesestudio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `planesestudio`
--

DROP TABLE IF EXISTS `planesestudio`;
CREATE TABLE `planesestudio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `planesestudio`
--

INSERT INTO `planesestudio` (`id`, `nombre`) VALUES
(1, 'Sala de 3 2015'),
(2, 'Sala de 4 2015'),
(3, 'Sala de 5 2015'),
(4, '1 Grado 2015'),
(5, '2 Grado 2015'),
(6, '3 Grado 2015'),
(7, '4 Grado 2015'),
(8, '5 Grado 2015'),
(9, '6 Grado 2015'),
(10, '1º Año 2015'),
(11, '2º Año 2015'),
(12, '3º Año 2015'),
(13, '4º Año HyS 2015'),
(14, '5º Año H y S 2015'),
(15, '6º Año H y S 2015'),
(16, '4º Año Nat 2015'),
(17, '5º Año NAT 2015'),
(18, '6º Año Nat 2015');

-- --------------------------------------------------------

--
-- Table structure for table `push`
--

DROP TABLE IF EXISTS `push`;
CREATE TABLE `push` (
  `id` bigint(20) NOT NULL,
  `token` varchar(128) NOT NULL,
  `fecha` timestamp NULL DEFAULT NULL,
  `plataforma` varchar(45) NOT NULL,
  `version` varchar(45) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `push`
--

INSERT INTO `push` (`id`, `token`, `fecha`, `plataforma`, `version`) VALUES
(1, '43b5ca3ae0e220fd6a3096bf49bbeed32c3f96bb', '2015-12-11 03:00:00', '', ''),
(2, '71f7c0c7088ba216b51e9cd3f80665094ffbb6b4', '2015-12-11 03:00:00', '', ''),
(3, 'c7a80795b8253db0767bba185ea6b195e8716b14', '2015-12-11 03:00:00', '', ''),
(4, '9ff1b21e8a9cf9fef1c07e7fdff2282e699396bf', '2015-12-11 03:00:00', '', ''),
(5, 'd033e22ae348aeb5660fc2140aec35850c4da997', '2015-12-11 03:00:00', '', ''),
(6, 'f885eddab5cc645645e797b542a1e3f6a7dbd310', '2015-12-11 03:00:00', '', ''),
(7, 'ef3a7ae69320740653295f4392d809d485195910', '2015-12-11 03:00:00', '', ''),
(8, '74b1557a1018006f0cf4a930294519e172d09510', '2015-12-11 03:00:00', '', ''),
(9, 'e030bb5b3e479d6b7b6ef3073d840fd7df2bc156', '2015-12-11 03:00:00', '', ''),
(10, 'dd324eed7a7a718116b779139b3ba13f3c34644b', '2015-12-11 03:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pushusers`
--

DROP TABLE IF EXISTS `pushusers`;
CREATE TABLE `pushusers` (
  `push_id` bigint(20) NOT NULL,
  `users_id` bigint(20) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pushusers`
--

INSERT INTO `pushusers` (`push_id`, `users_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 13),
(8, 14),
(9, 15),
(10, 16);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `description`, `deleted`) VALUES
(1, 'Administrator', 'Has full control over every aspect of the site.', 0),
(2, 'Editor', 'Can handle day-to-day management, but does not have full power.', 0),
(4, 'User', 'This is the default user with access to login.', 0),
(6, 'Developer', 'Developers typically are the only ones that can access the developer tools. Otherwise identical to Administrators, at least until the site is handed off.', 0),
(7, 'Super', 'Administrador del front end No de bonfire', 0),
(8, 'alumno', 'representa a los alumnos de la escuela, solo tiene permiso para consultar', 0),
(9, 'preceptores', 'abm asistencia, conducta y evaluaciones de recuperatorio', 0),
(10, 'departamento alumnos', 'Constulas', 0),
(11, 'Departamento Docentes', '', 0),
(12, 'Adocentes', '', 0),
(13, 'Tutores', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rolesusers`
--

DROP TABLE IF EXISTS `rolesusers`;
CREATE TABLE `rolesusers` (
  `roles_id` int(11) NOT NULL,
  `users_id` bigint(20) unsigned NOT NULL,
  `nodoDivision_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rolesusers`
--

INSERT INTO `rolesusers` (`roles_id`, `users_id`, `nodoDivision_id`) VALUES
(8, 3, 1),
(8, 4, 1),
(8, 5, 2),
(8, 6, 9),
(9, 1, 9),
(9, 2, 10),
(13, 13, 0),
(13, 14, 0),
(13, 15, 0),
(13, 16, 0);

-- --------------------------------------------------------

--
-- Table structure for table `suscripciones`
--

DROP TABLE IF EXISTS `suscripciones`;
CREATE TABLE `suscripciones` (
  `id` int(11) NOT NULL,
  `email` varchar(300) CHARACTER SET utf8 NOT NULL,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suscripciones`
--

INSERT INTO `suscripciones` (`id`, `email`, `fecha_hora`) VALUES
(11, 'sebastian@e-nodos.com', '2016-01-18 21:10:48'),
(12, 'francisco@e-nodos.com', '2016-01-18 21:13:56'),
(14, 'ecorona@bhp-global.com', '2016-01-18 23:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `tutoralumno`
--

DROP TABLE IF EXISTS `tutoralumno`;
CREATE TABLE `tutoralumno` (
  `alumno_id` bigint(20) unsigned NOT NULL,
  `tutor_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tutoralumno`
--

INSERT INTO `tutoralumno` (`alumno_id`, `tutor_id`) VALUES
(3, 1),
(3, 14),
(4, 14),
(3, 15),
(4, 15),
(5, 16),
(6, 16),
(5, 17),
(6, 17);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `foto` varchar(500) NOT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `timezone` char(4) NOT NULL DEFAULT 'UM6',
  `language` varchar(20) NOT NULL DEFAULT 'english'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `foto`, `banned`, `ban_message`, `timezone`, `language`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, 'yqzt6LoEzb8eraI3BQDgf.', 1268889823, 1454010942, 1, 'Admin', 'istrator', 'ADMIN', '0', '', 0, NULL, 'UM6', 'english'),
(3, '127.0.0.1', '22714926', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '22714926@admin.com', '', NULL, NULL, NULL, 'JzRr8ns1Ye3o8olcebeH3u', 1268889823, 1453251568, NULL, 'Andres', 'Gallardo', 'Nodos', NULL, 'http://e-nodos.com/img/cristian5.ico', 0, NULL, 'UM6', 'english'),
(4, '::1', NULL, '$2y$08$vH8EDvICH1JTAnsOefwdYe1.JdfEVhYGWmIe5gvptvK0xJP262Wba', NULL, 'alfredo@yahoo.com', NULL, NULL, NULL, NULL, 1454012763, NULL, 1, 'Alfredo Joaquin', 'Gomez', NULL, NULL, '', 0, NULL, 'UM6', 'english');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_viejo`
--

DROP TABLE IF EXISTS `users_viejo`;
CREATE TABLE `users_viejo` (
  `id` bigint(20) unsigned NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '4',
  `email` varchar(120) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password_hash` char(60) NOT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(40) NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `reset_by` int(10) DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  `display_name_changed` date DEFAULT NULL,
  `timezone` char(4) NOT NULL DEFAULT 'UM6',
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  `password_iterations` int(4) NOT NULL,
  `force_password_reset` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_viejo`
--

INSERT INTO `users_viejo` (`id`, `role_id`, `email`, `username`, `password_hash`, `reset_hash`, `last_login`, `last_ip`, `created_on`, `deleted`, `reset_by`, `banned`, `ban_message`, `display_name`, `display_name_changed`, `timezone`, `language`, `active`, `activate_hash`, `password_iterations`, `force_password_reset`) VALUES
(1, 1, 'admin@mybonfire.com', 'admin', '$2a$08$o9nfs8l/r2ijIelizQu9IO3limhYwk8Q5eQDDkHpqlm059iX4Uxvm', NULL, '2015-11-18 14:09:24', '186.159.120.238', '2014-06-09 00:53:27', 0, NULL, 0, NULL, 'admin', NULL, 'UM6', 'spanish_am', 1, '', 8, 0),
(2, 1, 'ignacio joaquín.acacio@gestionindustrial.edu.ar', '843641940', '$2a$08$HU8sc9xVRC7rhuuUSaFMI.jtkv36eXazYNnv0tVEX2s3zjrsHduoW', NULL, '2015-02-03 11:04:29', '186.159.122.2', '2014-08-13 15:19:37', 0, NULL, 0, NULL, 'Ignacio Joaquín ACACIO', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(3, 8, '844060554@gestionindustrial.edu.ar', '844060554', '$2a$08$QFMSG1ijESgS1NOpGLfDGepl0wjSHM1sPdWfoHKqkNCUZvuc8Zzc6', NULL, '2014-12-10 11:05:54', '181.23.197.250', '2014-08-13 15:19:37', 0, NULL, 0, NULL, '844060554', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(4, 8, '843489927@gestionindustrial.edu.ar', '843489927', '$2a$08$obr312YIn8zAtjY.2J/9HeC9pgN2NFFaEJ7v.ws/EC1AfeKbAd8x6', NULL, '2015-08-18 09:19:54', '186.39.139.69', '2014-08-13 15:19:37', 0, NULL, 0, NULL, '843489927', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(5, 8, '843763331@gestionindustrial.edu.ar', '843763331', '$2a$08$leG4WzF9QwvfkIwMTePoDuSGgfcAXpgUi.vDi32laK5ItgM76QQ/e', NULL, '2014-10-27 17:19:09', '190.179.165.204', '2014-08-13 15:19:37', 0, NULL, 0, NULL, '843763331', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(6, 8, '844316641@gestionindustrial.edu.ar', '844316641', '$2a$08$WLtrt4vUO/unHsQqkLz5muRFt5YNqQitb8yVhfiL8NSMSemCsmvey', NULL, '2014-12-06 10:52:16', '186.59.67.198', '2014-08-13 15:19:38', 0, NULL, 0, NULL, '844316641', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(7, 8, '843556034@gestionindustrial.edu.ar', '843556034', '$2a$08$iTIrlwwzXN/fcIGS/.B49OaYMwbkVtIPQExSLs2MJEr5VTGo/hh86', NULL, '2014-12-03 20:23:00', '186.39.194.117', '2014-08-13 15:19:38', 0, NULL, 0, NULL, '843556034', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(8, 8, ' santiago.fernandez gabri@gestionindustrial.edu.ar', '844061047', '$2a$08$DlExvUc/hJcZr6RBr4134enKShSKiJOjRoT2xonwkL6Slz/SMPtEm', NULL, '2014-12-17 07:42:03', '186.128.49.75', '2014-08-13 15:19:38', 0, NULL, 0, NULL, ' Santiago FERNANDEZ GABRI', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(9, 8, ' kevin edgar.garay jorquera@gestionindustrial.edu.ar', '843489600', '$2a$08$C2Y1puQgTbZ1W9jUDGaJn.Xp8EN7eKDb.RFgt2swgFyDzMkjuRB/.', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:19:38', 0, NULL, 0, NULL, ' Kevin Edgar GARAY JORQUERA', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(10, 8, ' nicolás.gioja@gestionindustrial.edu.ar', '843953398', '$2a$08$Llzqt0Y1YCjwSO7HM2cFSea0y43P7xe7GmMmCh/JncTW.zKLWfogq', NULL, '2014-12-03 08:59:35', '190.176.134.254', '2014-08-13 15:19:38', 0, NULL, 0, NULL, ' Nicolás GIOJA', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(11, 8, ' maximiliano andrés.lopez de la rosa@gestionindustrial.edu.ar', '844062017', '$2a$08$Brhz7m9wag5RdK0vjQb.m.aXKM7fzOOWVzXjoAQglHEoeCM432EVe', NULL, '2014-12-16 20:37:26', '186.59.21.231', '2014-08-13 15:19:38', 0, NULL, 0, NULL, ' Maximiliano Andrés LOPEZ de la ROSA', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(12, 8, ' lucia.maldonado echazu@gestionindustrial.edu.ar', '844061126', '$2a$08$2l6CcTvxu/ghWMOp3ICNZuegARKY3Vq3QXO8jcM0QPoGOV3bPRE1u', '24fbf66ee5a56d055e977032bfa599337b1ab3b6', '2015-04-01 09:24:01', '190.179.134.50', '2014-08-13 15:19:38', 0, 1418226162, 0, NULL, ' Lucia MALDONADO ECHAZU', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(13, 8, 'sergio agustín.martinez beatrice@gestionindustrial.edu.ar', '844062210', '$2a$08$ge35T4r9K5wrx3WTI9Fmqu6DwSKHFHVvzYaafSXA6KbjGckeF3Ptu', NULL, '2014-12-21 23:14:20', '190.179.207.153', '2014-08-13 15:19:38', 0, NULL, 0, NULL, 'Sergio Agustín MARTINEZ BEATRICE', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(14, 13, ' candelaria anahí.molina santander@gestionindustrial.edu.ar', '843763666', '$2a$08$JkjbA4m.kroo8aKJ93XvoOltPNXJs/5iDfqaU57Z7mHqBWXXwa3sq', NULL, '2014-12-12 14:45:58', '186.59.5.102', '2014-08-13 15:19:39', 0, NULL, 0, NULL, ' Candelaria Anahí MOLINA SANTANDER', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(15, 13, '844019097@gestionindustrial.edu.ar', '844019097', '$2a$08$GPw0nehEmqZHVpXvoC7CMukrRTOOXSgUVT/R9d8jY3hdf9cyX77bK', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:19:39', 0, NULL, 0, NULL, '844019097', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(16, 13, '843641898@gestionindustrial.edu.ar', '843641898', '$2a$08$rEaFrWQz/e5iWDOrBtAVpOOP94mbmiLI8Lu4QD1tDsqiaR0OEb8tm', NULL, '2014-11-26 20:29:56', '181.20.137.239', '2014-08-13 15:19:39', 0, NULL, 0, NULL, '843641898', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(17, 13, '843556497@gestionindustrial.edu.ar', '843556497', '$2a$08$urgiKbUA0EXZXzlDdvuCYOsNPN48eNaa2tjjPDoPtBrjsy1.9AkM.', NULL, '2014-11-26 09:46:23', '168.226.214.94', '2014-08-13 15:19:39', 0, NULL, 0, NULL, '843556497', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(18, 13, '843952761@gestionindustrial.edu.ar', '843952761', '$2a$08$0gzIVHXxsKsVnn9kTDbbfOYbzLg6OXkCRQMFVPxywBbm.vi8m9yXC', NULL, '2014-12-29 19:06:54', '186.39.221.215', '2014-08-13 15:19:39', 0, NULL, 0, NULL, '843952761', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(19, 13, '843764384@gestionindustrial.edu.ar', '843764384', '$2a$08$LgoQ8tal58hKKb1YFLt.v.CLcaVhscNT5keNtUQmzZODaUq0I47Yi', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:19:39', 0, NULL, 0, NULL, '843764384', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(20, 13, '843764388@gestionindustrial.edu.ar', '843764388', '$2a$08$31ey/2yi01L39IRy0v7cMObLc5XvKXc6LYlKs9EB0dRbJmg/8LrfS', NULL, '2014-12-15 10:10:54', '190.124.224.31', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '843764388', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(21, 13, '843642716@gestionindustrial.edu.ar', '843642716', '$2a$08$8tN.w5/tSFMJoUhFymneI.hHDWPcUA3gXtlytU23VvUh8zQI1quFO', NULL, '2014-12-06 10:30:47', '186.39.196.254', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '843642716', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(22, 13, '844316031@gestionindustrial.edu.ar', '844316031', '$2a$08$FGHj/zR2H4zX2ocy2PprdOWw4w1lXfYq3vRhKj3F78Iwkfw6AWb.K', NULL, '2014-11-29 13:55:15', '186.13.0.160', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '844316031', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(23, 8, '844061989@gestionindustrial.edu.ar', '844061989', '$2a$08$XaAmyJynG3SZH1e49JHJO.WLOPZUerXyr81Et3oTIfJ9nNlA392vy', NULL, '2015-08-20 11:04:31', '190.3.7.109', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '844061989', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(24, 8, '843952693@gestionindustrial.edu.ar', '843952693', '$2a$08$yGNRof2BI4iUwNCM68t2auKyMeJrZYteDwIl9Yr5WMDgwFrUc8sU.', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '843952693', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(25, 13, '843953135@gestionindustrial.edu.ar', '843953135', '$2a$08$e/m6bbRZkb3xI4Z6Vsh5Q.nKdL3Bw0NqklKyX73p.CaFDV/3Q6F2e', NULL, '2014-12-06 15:25:03', '190.179.234.40', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '843953135', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(26, 8, '844249428@gestionindustrial.edu.ar', '844249428', '$2a$08$hTWcMwMEnOvkWebJ5CMzOu7d3Knf9xfIt8.goetEX42nnw2MlvrCi', NULL, '2014-12-20 15:54:58', '186.39.185.83', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '844249428', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(27, 8, '844018999@gestionindustrial.edu.ar', '844018999', '$2a$08$UtNlHp09wV.Nl4QL1YFWKOVnmSpsDGKw.788fa2CDTo4b5dLnXqlO', NULL, '2014-12-14 01:25:00', '186.59.85.119', '2014-08-13 15:19:40', 0, NULL, 0, NULL, '844018999', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(28, 8, '843953357@gestionindustrial.edu.ar', '843953357', '$2a$08$Lfp43QZYv3kITBSHjJt6OunzTlGPBk2bxVuE4H.lPClVOW/r5NLom', NULL, '2014-12-04 15:20:16', '190.179.208.39', '2014-08-13 15:19:41', 0, NULL, 0, NULL, '843953357', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(29, 8, '844018944@gestionindustrial.edu.ar', '844018944', '$2a$08$HTdnExYWJhrt2DMzJQZ/Nujf36GNXTkdBjnOHtVzxPU2KweOOQiEy', NULL, '2014-12-11 16:06:06', '190.113.166.195', '2014-08-13 15:19:41', 0, NULL, 0, NULL, '844018944', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(30, 8, 'manuel maximiliano.verÓn vargas@gestionindustrial.edu.ar', '843764361', '$2a$08$AAxBc5AgL/Fd2OiloMhk3.88TVXjr2Kvk82bfcww5hG8ibqfLeP1y', NULL, '2014-12-20 13:57:33', '186.39.186.120', '2014-08-13 15:19:41', 0, NULL, 0, NULL, 'Manuel Maximiliano VERÓN VARGAS', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(31, 8, '843556492@gestionindustrial.edu.ar', '843556492', '$2a$08$FOkBPg8BcI2QIQECFNy4ueOELMMVp6F/W7Okgx5cHLnIKwndwK5zC', NULL, '2014-12-13 11:52:41', '186.39.155.10', '2014-08-13 15:52:49', 0, NULL, 0, NULL, '843556492', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(32, 8, '844127800@gestionindustrial.edu.ar', '844127800', '$2a$08$1.c7dGjc5b1d98Zz91aaze5VXiN.hM2oHDi8KtxcYW4iALu7kASqq', NULL, '2014-12-10 09:28:11', '186.39.134.124', '2014-08-13 15:52:49', 0, NULL, 0, NULL, '844127800', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(33, 8, '844019449@gestionindustrial.edu.ar', '844019449', '$2a$08$0KWI8UY6BELUbMkvE4DlseSU.cDHTJP849gvUDUAdcUBC5///pu0C', 'f64501e1adc052d59a6d743312410710177d330d', '2014-12-06 12:31:11', '186.39.191.38', '2014-08-13 15:52:49', 0, 1417644907, 0, NULL, '844019449', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(34, 8, ' bautista omar.baigorri,@gestionindustrial.edu.ar', '844126803', '$2a$08$IvtLLGcVvbMdfgeUb7W69e4j5viZPBSCUg7SE0Te0h6zFgoJXYJd6', NULL, '2014-12-10 10:06:59', '200.41.180.12', '2014-08-13 15:52:49', 0, NULL, 0, NULL, ' Bautista Omar BAIGORRI,', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(35, 8, '843952499@gestionindustrial.edu.ar', '843952499', '$2a$08$m25XFFhe0B824Q4NTSh4WeUp8S7JcM2RIdASvF6g9qJAiGMH0vtlK', NULL, '2014-12-18 18:52:05', '190.179.202.241', '2014-08-13 15:52:49', 0, NULL, 0, NULL, '843952499', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(36, 8, '843763788@gestionindustrial.edu.ar', '843763788', '$2a$08$qt10Eb/v90Ec416LWnn7..Zxby.DLFqonMssQfi4OUh82YNJUg9u2', NULL, '2014-12-09 11:58:06', '186.59.2.139', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '843763788', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(37, 8, '843488692@gestionindustrial.edu.ar', '843488692', '$2a$08$zXTkORLAM7jCKJYTzsKWReILg4fAyrtXbmnF9Appb92Xz4SJ45bFa', NULL, '2014-12-07 21:54:52', '186.39.221.137', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '843488692', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(38, 8, '843998999@gestionindustrial.edu.ar', '843998999', '$2a$08$106Q3kVOR2vcny5OOJTphOm1QEiPcp5OIrdBKNOPxfvNQ7kRMzd4q', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '843998999', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(39, 8, '844432076@gestionindustrial.edu.ar', '844432076', '$2a$08$HpAJyRB1XbZYJPrTAaj2HutyTUPF3Vq0dOXeip8asO/VcrqSN.HEu', NULL, '2014-12-28 19:04:19', '54.244.58.239', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '844432076', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(40, 8, '843556014@gestionindustrial.edu.ar', '843556014', '$2a$08$LNxj1IWT1QzUDM8gzFF22.CGa4Q..1pqQ.FuhBKP/oX5CYgP1C.RG', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '843556014', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(41, 8, '844127940@gestionindustrial.edu.ar', '844127940', '$2a$08$NOgqBSt51V7FOaeOkaIiqOCD1IlhBuRq0OFsoC1FVaxfAPlVk6OFe', NULL, '2014-12-15 13:21:43', '200.81.38.209', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '844127940', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(42, 8, '843839098@gestionindustrial.edu.ar', '843839098', '$2a$08$jjQzVnfMtBPbjpvf5p7spOYeFxyamftCCP53zSNAzkgFxODY0ySCe', NULL, '2014-12-01 19:00:36', '190.179.194.216', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '843839098', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(43, 8, '843556025@gestionindustrial.edu.ar', '843556025', '$2a$08$LdrI3N.ZRhmucJeyakVb6usgHfRE.Q02k0nD/ky7p9XrHChpBkQbi', NULL, '2014-12-08 12:13:00', '186.128.28.243', '2014-08-13 15:52:50', 0, NULL, 0, NULL, '843556025', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(44, 8, '843556276@gestionindustrial.edu.ar', '843556276', '$2a$08$ScVIpwf7NBAXLJkivLDUw.EXhUF3F1piPs0LY2I.bqF1j1V9lkCjm', NULL, '2014-12-05 20:31:16', '190.179.251.57', '2014-08-13 15:52:51', 0, NULL, 0, NULL, '843556276', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(45, 8, '843764385@gestionindustrial.edu.ar', '843764385', '$2a$08$VXsNGTmdQPJVEibHfskBiuh9t5WCqYmbrihXLixm3KJSlzbszDFee', NULL, '2014-12-05 11:11:15', '200.5.203.42', '2014-08-13 15:52:51', 0, NULL, 0, NULL, '843764385', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(46, 8, '844249937@gestionindustrial.edu.ar', '844249937', '$2a$08$JQ2jgAxpvdpEsQew5txtWejBJphbouQw9G8ecJbw7VakNumGUt2lO', 'f9c021c5963fb54a5420d8c5892f4b83361643e7', '2014-12-04 19:39:58', '181.109.225.9', '2014-08-13 15:52:51', 0, 1417485258, 0, NULL, '844249937', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(47, 8, '844127251@gestionindustrial.edu.ar', '844127251', '$2a$08$a4/BD52SMoTvF.UKF4DY7ulz9n83aBfx4T4URqCdehabyVVbeWeBO', NULL, '2014-11-30 18:47:16', '190.179.153.58', '2014-08-13 15:52:51', 0, NULL, 0, NULL, '844127251', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(48, 8, '843952789@gestionindustrial.edu.ar', '843952789', '$2a$08$F17vag0UmRXmwnPHM/xj3..nJ94rdtfTYDUnwallMQiGO9qpUU6KS', NULL, '2014-12-15 12:21:01', '190.179.242.39', '2014-08-13 15:52:51', 0, NULL, 0, NULL, '843952789', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(49, 8, ' gonzalo agustin.ortega quinteros,@gestionindustrial.edu.ar', '843839134', '$2a$08$FH8QtBNtGAXq1bR0zK5ZUO7oCTiVr7WSftPdoPzdlHUtXR9pcFsjS', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:52:51', 0, NULL, 0, NULL, ' Gonzalo Agustin ORTEGA QUINTEROS,', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(50, 8, '843556685@gestionindustrial.edu.ar', '843556685', '$2a$08$CAGtNyBy853Upu3XYd8TFOEMJJhuMnAsNONoY3QPOsj2PAdj0rPge', NULL, '2014-11-27 18:07:44', '190.179.231.19', '2014-08-13 15:52:51', 0, NULL, 0, NULL, '843556685', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(51, 8, ' matías.putruele dÍaz,@gestionindustrial.edu.ar', '843952720', '$2a$08$nhggIKP90FGJje.jrb4mNOrn/4qAumVCesmrjRuUxz75IfXQWJT6i', NULL, '2015-03-10 06:57:40', '200.0.236.252', '2014-08-13 15:52:52', 0, NULL, 0, NULL, ' Matías PUTRUELE DÍAZ,', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(52, 8, '843642982@gestionindustrial.edu.ar', '843642982', '$2a$08$GCE3YVcezqtkBvtWY5SPNuGdOlATKVzSTkboBriKp5N8kdTR/S9TC', '829967e1a63ee05778c2c5f22d1b21a140a33331', '2014-12-11 09:39:41', '190.176.160.20', '2014-08-13 15:52:52', 0, 1415986986, 0, NULL, '843642982', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(53, 8, '844062248@gestionindustrial.edu.ar', '844062248', '$2a$08$iLq/s67qsm3.nR1wQcmTa.bTKzVnjK4GhEAvG18Cr6DCJI5QGmzL.', NULL, '2014-12-15 07:22:50', '190.124.224.31', '2014-08-13 15:52:52', 0, NULL, 0, NULL, '844062248', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(54, 8, '844249728@gestionindustrial.edu.ar', '844249728', '$2a$08$MdE3.sTK2dK.d5msnfIR6.04nEIhcERTs7uOI8VWB7bFviulmyTPi', NULL, '2015-03-06 06:40:01', '186.59.4.245', '2014-08-13 15:52:52', 0, NULL, 0, NULL, '844249728', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(55, 8, '843763304@gestionindustrial.edu.ar', '843763304', '$2a$08$U5cUQv0sBJIRMdsQ7uvTcuiJcAflmwDA895OCBByfXE2dQTd9toAO', NULL, '2014-12-01 08:54:46', '181.225.28.43', '2014-08-13 15:52:52', 0, NULL, 0, NULL, '843763304', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(56, 8, '844127785@gestionindustrial.edu.ar', '844127785', '$2a$08$bHb.mHwqGVu28DAJtE3EueyQSogZX96o8wUA7nJzA2ZvpPYRPn3.q', NULL, '2014-11-27 23:06:24', '186.128.91.235', '2014-08-13 15:52:52', 0, NULL, 0, NULL, '844127785', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(57, 8, ' lourdes milagro.aguilera godoy,@gestionindustrial.edu.ar', '844126523', '$2a$08$.kuw55fBzlpVucPHXcuXmuVO7FwoN.Hc8ggjAlxWe1qFepihr3Bq.', NULL, '2014-12-05 15:27:06', '181.225.28.6', '2014-08-13 15:57:47', 0, NULL, 0, NULL, ' Lourdes Milagro AGUILERA GODOY,', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(58, 8, ' jeremías nahuel.aguilera herrero@gestionindustrial.edu.ar', '844018979', '$2a$08$6M8gLvt8CnWTO/TlWVpDUu8wfAISLULDv1T9tpXVAa14uhsibbrDy', NULL, '2015-03-07 12:36:50', '186.12.226.171', '2014-08-13 15:57:47', 0, NULL, 0, NULL, ' Jeremías Nahuel AGUILERA HERRERO', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(59, 8, ' francisco david.arabena agÜero,@gestionindustrial.edu.ar', '843556018', '$2a$08$olI62CU13uU40KJTHZxaFuSJ4FXGRep0viE3eqyCDy7hf/LSaXxRS', NULL, '2014-12-01 15:26:52', '181.225.28.8', '2014-08-13 15:57:47', 0, NULL, 0, NULL, ' Francisco David ARABENA AGÜERO,', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(60, 8, 'julián santiago.armada@gestionindustrial.edu.ar', '843556277', '$2a$08$gyYtfYBAJv/CuwFv3Uy6T.3Y8FH55XWaX/QgjCjzqHhkgNzMulZ3K', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:57:47', 0, NULL, 0, NULL, 'Julián Santiago ARMADA', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(61, 8, ' santiago agustín.barrios@gestionindustrial.edu.ar', '843689281', '$2a$08$VDvkCmKUXX6sMMJ.DK0kDeg4XloXBY0xpUqqsjh4bIlIDMVyjYJKG', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:57:47', 0, NULL, 0, NULL, ' Santiago Agustín BARRIOS', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(62, 8, ' julieta florencia.capellino araoz@gestionindustrial.edu.ar', '843642492', '$2a$08$iiXkA7OIcsJPsprOEPqL6OreunIZ55VmQKMnOA5VEvRwhw3eo4YB6', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:57:47', 0, NULL, 0, NULL, ' Julieta Florencia CAPELLINO ARAOZ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(63, 8, 'romina tamara.conca@gestionindustrial.edu.ar', '844019409', '$2a$08$2HDgdkwcBi0Rq18GuK3Z8.JLoeqVTGQjGzQsZ9uQPyu6wVLOBnm9G', NULL, '2014-12-04 20:43:52', '190.179.207.90', '2014-08-13 15:57:47', 0, NULL, 0, NULL, 'Romina Tamara CONCA', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(64, 8, ' iván alejandro.conte grand@gestionindustrial.edu.ar', '843642755', '$2a$08$8.rLOhymyhV.pK46CnDWHu9FTfEF0zeyBX1MWmNxTR6m5B/M13Ovu', NULL, '2014-12-10 10:39:54', '186.128.66.210', '2014-08-13 15:57:47', 0, NULL, 0, NULL, ' Iván Alejandro CONTE GRAND', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(65, 8, 'hugo josé daniel.dÍaz ortiz, @gestionindustrial.edu.ar', '843556036', '$2a$08$rjIsADdkvdJDbv3mpUqSjeoVZzY.u0krCAPMl5Sp0Y7d.Enw1Wduu', NULL, '2014-12-22 06:36:13', '181.108.36.98', '2014-08-13 15:57:48', 0, NULL, 0, NULL, 'Hugo José Daniel DÍAZ ORTIZ, ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(66, 8, 'juan pablo.elizondo dÍaz@gestionindustrial.edu.ar', '843839096', '$2a$08$9p2Zknd9nSEu1jJUoNbxRu/kiGUF96vUAmbCOeF9DPoebdRhxZGN6', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:57:48', 0, NULL, 0, NULL, 'Juan Pablo ELIZONDO DÍAZ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(67, 8, 'micaela alejandra.flores@gestionindustrial.edu.ar', '843689925', '$2a$08$bjw0ZofWqny5J6j3cTzkzO2WSGsA0JXSEHflQDuVGYQR35r33tyS6', NULL, '2014-12-11 10:57:42', '181.23.196.7', '2014-08-13 15:57:48', 0, NULL, 0, NULL, 'Micaela Alejandra FLORES', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(68, 8, ' agustina victoria.garces posleman@gestionindustrial.edu.ar', '844060507', '$2a$08$FOIupLDzJPBsbMXcqjpQQOkg0IhCo7Ku9q7uozmvsaLLC.5z9XJ7y', NULL, '2014-12-15 07:08:48', '190.179.172.134', '2014-08-13 15:57:48', 0, NULL, 0, NULL, ' Agustina Victoria GARCES POSLEMAN', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(69, 8, ' adriano lucas.gÓmez@gestionindustrial.edu.ar', '843689131', '$2a$08$KkHljtV9OX6o6.UZL5RLgeir26a46GNqFBBkDIvBr6FMH0jyUCq26', NULL, '2014-12-08 18:55:36', '190.179.231.29', '2014-08-13 15:57:48', 0, NULL, 0, NULL, ' Adriano Lucas GÓMEZ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(70, 8, ' ana carolina.iglesias garcÍa@gestionindustrial.edu.ar', '844060510', '$2a$08$etKE5tmBkyClROwnRfVlseIR.0Hdw0fvdTrvjwkGeOEqgKgVS/Cti', NULL, '2014-12-04 13:42:34', '190.179.229.156', '2014-08-13 15:57:48', 0, NULL, 0, NULL, ' Ana Carolina IGLESIAS GARCÍA', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(71, 8, 'mariano andrés.martinez moreno@gestionindustrial.edu.ar', '843764228', '$2a$08$rgB9286HmT6Gj5Gi6tTHk.iH6mTnPchhSKylN8WZwplz.qbxlECFe', NULL, '2014-12-02 20:06:56', '200.114.97.21', '2014-08-13 15:57:48', 0, NULL, 0, NULL, 'Mariano Andrés MARTINEZ MORENO', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(72, 8, ' juan cruz.martinez sanchez@gestionindustrial.edu.ar', '843556540', '$2a$08$2HaLu8sS6m333YVQpVyh2uZLQaojTmFyJ4TDLidjq5tuF1god8YRS', NULL, '2014-12-11 10:23:45', '190.179.231.57', '2014-08-13 15:57:49', 0, NULL, 0, NULL, ' Juan Cruz MARTINEZ SANCHEZ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(73, 8, 'matías gastón.mercado neder@gestionindustrial.edu.ar', '843689026', '$2a$08$QRSQa5Wi5/UifWIThe/0BuDTqMjKiOVfczrrCYC2zabfQWcOgdOCW', NULL, '2014-12-15 06:49:04', '190.221.213.43', '2014-08-13 15:57:49', 0, NULL, 0, NULL, 'Matías Gastón MERCADO NEDER', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(74, 8, 'valentino miguel.miolano campanello@gestionindustrial.edu.ar', '843953307', '$2a$08$MMR8hWi/6wqDLFQ4aD.BE.NQMQpUAmWj0DZp4AdY0m9SON/mzlCRi', NULL, '2014-12-08 12:54:50', '186.59.62.147', '2014-08-13 15:57:49', 0, NULL, 0, NULL, 'Valentino Miguel MIOLANO CAMPANELLO', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(75, 8, ' francisco david.molina gutierrez@gestionindustrial.edu.ar', '844127112', '$2a$08$d63dZ8eVLkFp2hLqIv7oGuAxYQzDtM7v34eAuZKWIbDPv.X6/qiI6', NULL, '2014-12-05 21:04:05', '186.140.144.67', '2014-08-13 15:57:49', 0, NULL, 0, NULL, ' Francisco David MOLINA GUTIERREZ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(76, 8, 'guillermo luis.montiveros@gestionindustrial.edu.ar', '844018930', '$2a$08$MA3TW7eaRlKwITCRY1hV6.e1ruyI/zZfCw6WvGBvzM5I4WgTEakDa', NULL, '2014-12-23 11:56:15', '190.221.213.129', '2014-08-13 15:57:49', 0, NULL, 0, NULL, 'Guillermo Luis MONTIVEROS', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(77, 8, 'samuel.nuÑez molini@gestionindustrial.edu.ar', '844249502', '$2a$08$AvrWi9iYIhpldMD8.jgFP.4Qxr51s87klFmEf7BuJLFzef..EzQqK', NULL, '2015-02-20 20:10:06', '190.179.214.182', '2014-08-13 15:57:49', 0, NULL, 0, NULL, 'Samuel NUÑEZ MOLINI', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(78, 8, 'gonzalo agustín.ortiz@gestionindustrial.edu.ar', '843838393', '$2a$08$J4Id2fH/.cR.9hSWcEn24ONLgrJsN3f63R/fO3RuefvyvGGZ56h0i', NULL, '2014-12-05 07:45:05', '200.81.44.144', '2014-08-13 15:57:49', 0, NULL, 0, NULL, 'Gonzalo Agustín ORTIZ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(79, 8, ' martín alberto.perisotto@gestionindustrial.edu.ar', '844062215', '$2a$08$iIwXW32L.qRD4pYGa6EQ.eL7k..91d6zr66lPjjAy8chwhSfkSepm', NULL, '2014-12-04 06:25:38', '190.124.224.31', '2014-08-13 15:57:50', 0, NULL, 0, NULL, ' Martín Alberto PERISOTTO', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(80, 8, ' lourdes agustina.pinto sarmiento@gestionindustrial.edu.ar', '843642478', '$2a$08$xHb0RETMR8Bul7mfIx9VeOgSgbD52QE5x3jFRedLcw9qFbP6fxivq', NULL, '0000-00-00 00:00:00', '', '2014-08-13 15:57:50', 0, NULL, 0, NULL, ' Lourdes Agustina PINTO SARMIENTO', NULL, 'UM3', 'spanish_am', 1, '', 8, 0),
(81, 8, ' lautaro.salinas gomez@gestionindustrial.edu.ar', '844249418', '$2a$08$lfhqnffRJpXJYsfaf0j7YOS0FPtixaDHg25Q/2jX3TpPJRKZB.wHG', NULL, '2014-12-01 10:05:05', '186.128.8.235', '2014-08-13 15:57:50', 0, NULL, 0, NULL, ' Lautaro SALINAS GOMEZ', NULL, 'UM3', 'spanish_am', 1, '', 8, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anios`
--
ALTER TABLE `anios`
  ADD PRIMARY KEY (`id`,`niveles_id`),
  ADD KEY `fk_anios_niveles1_idx` (`niveles_id`);

--
-- Indexes for table `cicloa`
--
ALTER TABLE `cicloa`
  ADD PRIMARY KEY (`id`,`planesestudio_id`),
  ADD KEY `fk_cicloa_planesestudio1_idx` (`planesestudio_id`);

--
-- Indexes for table `destino`
--
ALTER TABLE `destino`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dias`
--
ALTER TABLE `dias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisiones`
--
ALTER TABLE `divisiones`
  ADD PRIMARY KEY (`id`,`anios_id`),
  ADD KEY `fk_divisiones_anios1_idx` (`anios_id`);

--
-- Indexes for table `especialidades`
--
ALTER TABLE `especialidades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`,`users_id`),
  ADD KEY `fk_eventos_users1_idx` (`users_id`);

--
-- Indexes for table `eventosdestino`
--
ALTER TABLE `eventosdestino`
  ADD PRIMARY KEY (`eventos_id`,`destino_id`),
  ADD KEY `fk_eventos_has_destino_destino1_idx` (`destino_id`),
  ADD KEY `fk_eventos_has_destino_eventos1_idx` (`eventos_id`);

--
-- Indexes for table `eventosusers`
--
ALTER TABLE `eventosusers`
  ADD PRIMARY KEY (`eventos_id`,`users_id`),
  ADD KEY `fk_eventos_has_users_users1_idx` (`users_id`),
  ADD KEY `fk_eventos_has_users_eventos1_idx` (`eventos_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `horariosmaterias`
--
ALTER TABLE `horariosmaterias`
  ADD PRIMARY KEY (`horarios_id`,`materias_id`,`dias_id`,`divisiones_id`),
  ADD KEY `fk_horarios_materias_horarios1_idx` (`horarios_id`),
  ADD KEY `fk_horarios_materias_materias1_idx` (`materias_id`),
  ADD KEY `fk_horarios_materias_dias1_idx` (`dias_id`),
  ADD KEY `fk_horariosMaterias_divisiones1_idx` (`divisiones_id`);

--
-- Indexes for table `inscripciones`
--
ALTER TABLE `inscripciones`
  ADD PRIMARY KEY (`id`,`planesestudio_id`,`divisiones_id`),
  ADD KEY `fk_inscripciones_planesestudio1_idx` (`planesestudio_id`),
  ADD KEY `fk_inscripciones_divisiones1_idx` (`divisiones_id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `limits`
--
ALTER TABLE `limits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id`,`planesestudio_id`,`anios_id`),
  ADD KEY `fk_materias_planesestudio1_idx` (`planesestudio_id`),
  ADD KEY `fk_materias_anios1_idx` (`anios_id`);

--
-- Indexes for table `niveles`
--
ALTER TABLE `niveles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nodocolegio`
--
ALTER TABLE `nodocolegio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nododivision`
--
ALTER TABLE `nododivision`
  ADD PRIMARY KEY (`id`,`nodoColegio_id`,`planesestudio_id`,`divisiones_id`),
  ADD KEY `fk_nodoDivision_nodoColegio1_idx` (`nodoColegio_id`),
  ADD KEY `fk_nodoDivision_planesestudio1_idx` (`planesestudio_id`),
  ADD KEY `fk_nodoDivision_divisiones1_idx` (`divisiones_id`);

--
-- Indexes for table `nombresmateria`
--
ALTER TABLE `nombresmateria`
  ADD PRIMARY KEY (`id`,`materias_id`),
  ADD KEY `fk_nombresMateria_materias1_idx` (`materias_id`);

--
-- Indexes for table `periodosevaluacion`
--
ALTER TABLE `periodosevaluacion`
  ADD PRIMARY KEY (`id`,`planesestudio_id`),
  ADD KEY `fk_periodosevaluacion_planesestudio1_idx` (`planesestudio_id`);

--
-- Indexes for table `planesestudio`
--
ALTER TABLE `planesestudio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push`
--
ALTER TABLE `push`
  ADD PRIMARY KEY (`id`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `pushusers`
--
ALTER TABLE `pushusers`
  ADD PRIMARY KEY (`push_id`,`users_id`),
  ADD KEY `fk_push_has_users_users1_idx` (`users_id`),
  ADD KEY `fk_push_has_users_push1_idx` (`push_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `rolesusers`
--
ALTER TABLE `rolesusers`
  ADD PRIMARY KEY (`roles_id`,`users_id`,`nodoDivision_id`),
  ADD KEY `fk_roles_has_users_users1_idx` (`users_id`),
  ADD KEY `fk_roles_has_users_roles1_idx` (`roles_id`),
  ADD KEY `fk_roles_users_nodoDivision1_idx` (`nodoDivision_id`);

--
-- Indexes for table `suscripciones`
--
ALTER TABLE `suscripciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `email_2` (`email`);

--
-- Indexes for table `tutoralumno`
--
ALTER TABLE `tutoralumno`
  ADD PRIMARY KEY (`alumno_id`,`tutor_id`),
  ADD KEY `fk_users_has_users_users2_idx` (`tutor_id`),
  ADD KEY `fk_users_has_users_users1_idx` (`alumno_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `users_viejo`
--
ALTER TABLE `users_viejo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `anios`
--
ALTER TABLE `anios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cicloa`
--
ALTER TABLE `cicloa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dias`
--
ALTER TABLE `dias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `divisiones`
--
ALTER TABLE `divisiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `especialidades`
--
ALTER TABLE `especialidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `inscripciones`
--
ALTER TABLE `inscripciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `limits`
--
ALTER TABLE `limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=659;
--
-- AUTO_INCREMENT for table `materias`
--
ALTER TABLE `materias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `niveles`
--
ALTER TABLE `niveles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nododivision`
--
ALTER TABLE `nododivision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `nombresmateria`
--
ALTER TABLE `nombresmateria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `periodosevaluacion`
--
ALTER TABLE `periodosevaluacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `planesestudio`
--
ALTER TABLE `planesestudio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `push`
--
ALTER TABLE `push`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `suscripciones`
--
ALTER TABLE `suscripciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users_viejo`
--
ALTER TABLE `users_viejo`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
