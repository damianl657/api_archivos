-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-07-2016 a las 23:23:07
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nodos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_acciones`
--

CREATE TABLE IF NOT EXISTS `menu_acciones` (
`id` int(11) NOT NULL,
  `nombre` text,
  `metodo` text NOT NULL,
  `descripcion` text,
  `link` text NOT NULL,
  `tipo` text NOT NULL,
  `id_accion_boton` text NOT NULL,
  `orden` text NOT NULL,
  `icono` text,
  `estado` int(11) NOT NULL,
  `controlador` text NOT NULL,
  `id_key` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_acciones`
--

INSERT INTO `menu_acciones` (`id`, `nombre`, `metodo`, `descripcion`, `link`, `tipo`, `id_accion_boton`, `orden`, `icono`, `estado`, `controlador`, `id_key`) VALUES
(1, 'Home', 'home', 'nodosweb', 'Login/home', 'menu', '', '1', 'fa fa-home', 1, 'Login', 3),
(2, 'Calendario', 'index', 'nodosweb', 'Calendario/index', 'menu', '', '2', 'fa fa-calendar', 1, 'Calendario', 3),
(4, 'Horarios', 'index', 'nodosweb', 'Horarios/index', 'menu', '', '3', 'fa fa-clock-o', 1, 'Horarios', 3),
(5, 'editar_calendario', '', 'api', '', 'accion', '', '', NULL, 1, '', 3),
(6, 'Editar Perfil', '', 'boton_editar_perfil_nodosweb', '', 'boton', '', '', 'fa fa-edit', 1, '', 3),
(7, 'Cambiar Contraseña', '', 'cambiar_contraseña_nodosweb', '', 'boton', 'cambiar_contraseña', '', NULL, 1, '', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menu_acciones`
--
ALTER TABLE `menu_acciones`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menu_acciones`
--
ALTER TABLE `menu_acciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
