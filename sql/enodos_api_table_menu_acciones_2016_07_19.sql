-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-07-2016 a las 21:22:41
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nodos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_acciones`
--

CREATE TABLE IF NOT EXISTS `menu_acciones` (
`id` int(11) NOT NULL,
  `nombre` text,
  `metodo` text NOT NULL,
  `descripcion` text,
  `link` text NOT NULL,
  `tipo` text NOT NULL,
  `orden` text NOT NULL,
  `icono` text,
  `estado` int(11) NOT NULL,
  `controlador` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_acciones`
--

INSERT INTO `menu_acciones` (`id`, `nombre`, `metodo`, `descripcion`, `link`, `tipo`, `orden`, `icono`, `estado`, `controlador`) VALUES
(1, 'Home', 'home', 'nodosweb', 'Login/home', 'menu', '1', 'fa fa-home', 1, 'Login'),
(2, 'Calendario', 'index', 'nodosweb', 'Calendario/index', 'menu', '2', 'fa fa-calendar', 1, 'Calendario'),
(4, 'Horarios', 'index', 'nodosweb', 'Horarios/index', 'menu', '3', 'fa fa-clock-o', 1, 'Horarios');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `menu_acciones`
--
ALTER TABLE `menu_acciones`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `menu_acciones`
--
ALTER TABLE `menu_acciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
