CREATE TABLE `eventosusers_detalle` 
( `id` INT NOT NULL AUTO_INCREMENT , 
	`eventosusers_id` INT NOT NULL , 
	`token_id` INT NOT NULL , 
	`visto` INT NULL DEFAULT '0' , 
	`aceptado` INT NULL DEFAULT '0' , 
	`enviado` INT NOT NULL DEFAULT '0' , 
	`medio` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
	`alta` VARCHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
	`fechaConfirmacion` DATETIME NULL DEFAULT NULL , 
	`fechaVisto` DATETIME NULL DEFAULT NULL , 
	`me_gusta` INT NOT NULL DEFAULT '0' , 
	`fechaMegusta` DATETIME NULL DEFAULT NULL , 
	PRIMARY KEY (`id`)) 
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;