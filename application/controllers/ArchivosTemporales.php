<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    define("SIZE_EVENT", 20);
    define("SIZE_FOTO", 10);


    class ArchivosTemporales extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_upload");
    }
    public function getFilesXruta_post()
    {
        $userid = $this->utilidades->verifica_ingreso_externo();
        //echo $userid; die(); 
        
        if ($userid == -1) {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        } 
        else 
        {
            $files = $_POST;
            //$files = json_decode($files);
            $filesUrl = array();
            foreach ($files as $key ) {
                # code...
                $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$key;
                echo "ruta: ".$ruta;
                $nombrefile = explode('/', $key);
                $nombrefile = $nombrefile[1];
                $file = explode('_', $key);
                if (file_exists($ruta))
                {
                    $ext = pathinfo($ruta, PATHINFO_EXTENSION);
                    
                        copy($ruta, $_SERVER['DOCUMENT_ROOT'].'/cloudtmp/'.$nombrefile );
                        $filesUrl[] =  array( 'id' => $file[0], 'file' => '/cloudtmp/'.$nombrefile);
                }
            }
             $this->response([
                    'status' => TRUE,
                    'files' => $filesUrl,
                ], REST_Controller::HTTP_OK);

        }
    }

}