<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    define("SIZE_EVENT", 20);
    define("SIZE_FOTO", 10);


    class Filesbase64 extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_upload");
    }
    public function getFilesXruta_post()
   	{
   		$userid = $this->utilidades->verifica_ingreso_externo();
        //echo $userid; die(); 
        
        if ($userid == -1) {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        } 
        else 
        {
        	/*$files = $_POST['rutas'];
        	foreach ($files as $ruta) {
        		if (file_exists($ruta))
        		{

        		}
        		# code...
        	}*/
        	$ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/2021-04/'.$_POST['ruta'];
            //echo "ruta: ".$ruta;
        	if (file_exists($ruta))
        	{
        		$ext = pathinfo($ruta, PATHINFO_EXTENSION);
                
                //$file_ = file_get_contents($ruta);
                    //$data = base64_encode($file_);
                    //desde a
                    //$fileBase64 = 'data:'.$file['type'].';base64,'.$data;
                    //$fileBase64 = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,'.$data;
                    copy($ruta, $_SERVER['DOCUMENT_ROOT'].'/cloudtmp/'.$_POST['ruta'] );
                    //print_r($fileBase64);
                    
        	}
        }
   	}

}