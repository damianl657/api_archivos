<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    define("SIZE_EVENT", 20);
    define("SIZE_FOTO", 10);


    class Upload extends REST_Controller {


    private $extPermitidas = array('jpg','jpeg','png','gif','bmp', 'doc','dot','rtf','docx','xlsx','xls','csv','pdf','rar','zip', '7z','gz','tgz','txt','ppt','pps','pptx','potx','ppsx','odt','ods','mp3','wma','JPG','JPEG','PNG','BMP','mp4','MP4','MPEG4','Ogg','m4a','aiff','iff', 'AIF', 'AIFC', 'AIFF', 'AMF', 'ASF', 'AU', 'AUDIOCD', 'CDA', 'CDDA', 'FAR', 'IT', 'ITZ', 'LWV', 'MID', 'MIDI', 'MIZ', 'MP1', 'MP2','MP3','MTM','OGG','OGM','OKT','RA','RMI','SND','STM','STZ','ULT','VOC','WAV','WAX','WM','WMA','WMV','XM','XMZ','ogg','aac','DOCX','DOC','opus','sb3');

    private $extPermitidasFotos = array('jpg','jpeg','png','bmp','JPG','JPEG','PNG','BMP');


    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_upload");
        //$this->load->library("class.uploader");
       
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }

    /*public function get_extPermitidas(){
        return $this->extPermitidas;
    }*/
    public function quitar_emojis($texto){
    return preg_replace('/[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0077}\x{E006C}\x{E0073}\x{E007F})|[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0073}\x{E0063}\x{E0074}\x{E007F})|[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0065}\x{E006E}\x{E0067}\x{E007F})|[\x{1F3F4}](?:\x{200D}\x{2620}\x{FE0F})|[\x{1F3F3}](?:\x{FE0F}\x{200D}\x{1F308})|[\x{0023}\x{002A}\x{0030}\x{0031}\x{0032}\x{0033}\x{0034}\x{0035}\x{0036}\x{0037}\x{0038}\x{0039}](?:\x{FE0F}\x{20E3})|[\x{1F441}](?:\x{FE0F}\x{200D}\x{1F5E8}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F466})|[\x{1F469}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F469})|[\x{1F469}\x{1F468}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F468})|[\x{1F469}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F48B}\x{200D}\x{1F469})|[\x{1F469}\x{1F468}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F48B}\x{200D}\x{1F468})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B0})|[\x{1F575}\x{1F3CC}\x{26F9}\x{1F3CB}](?:\x{FE0F}\x{200D}\x{2640}\x{FE0F})|[\x{1F575}\x{1F3CC}\x{26F9}\x{1F3CB}](?:\x{FE0F}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FF}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FE}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FD}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FC}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FB}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F9B8}\x{1F9B9}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F9DE}\x{1F9DF}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F46F}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93C}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FF}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FE}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FD}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FC}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FB}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F9B8}\x{1F9B9}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F9DE}\x{1F9DF}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F46F}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93C}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{200D}\x{2642}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2695}\x{FE0F})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FF})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FE})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FD})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FC})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FB})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F8}\x{1F1F9}\x{1F1FA}](?:\x{1F1FF})|[\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F0}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1FA}](?:\x{1F1FE})|[\x{1F1E6}\x{1F1E8}\x{1F1F2}\x{1F1F8}](?:\x{1F1FD})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F7}\x{1F1F9}\x{1F1FF}](?:\x{1F1FC})|[\x{1F1E7}\x{1F1E8}\x{1F1F1}\x{1F1F2}\x{1F1F8}\x{1F1F9}](?:\x{1F1FB})|[\x{1F1E6}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1ED}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F7}\x{1F1FB}](?:\x{1F1FA})|[\x{1F1E6}\x{1F1E7}\x{1F1EA}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FE}](?:\x{1F1F9})|[\x{1F1E6}\x{1F1E7}\x{1F1EA}\x{1F1EC}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F7}\x{1F1F8}\x{1F1FA}\x{1F1FC}](?:\x{1F1F8})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EB}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F0}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1F7})|[\x{1F1E6}\x{1F1E7}\x{1F1EC}\x{1F1EE}\x{1F1F2}](?:\x{1F1F6})|[\x{1F1E8}\x{1F1EC}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F3}](?:\x{1F1F5})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1EE}\x{1F1EF}\x{1F1F2}\x{1F1F3}\x{1F1F7}\x{1F1F8}\x{1F1F9}](?:\x{1F1F4})|[\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}](?:\x{1F1F3})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F4}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FF}](?:\x{1F1F2})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1EE}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1F1})|[\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1ED}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FD}](?:\x{1F1F0})|[\x{1F1E7}\x{1F1E9}\x{1F1EB}\x{1F1F8}\x{1F1F9}](?:\x{1F1EF})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EB}\x{1F1EC}\x{1F1F0}\x{1F1F1}\x{1F1F3}\x{1F1F8}\x{1F1FB}](?:\x{1F1EE})|[\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1ED})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EA}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}](?:\x{1F1EC})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F9}\x{1F1FC}](?:\x{1F1EB})|[\x{1F1E6}\x{1F1E7}\x{1F1E9}\x{1F1EA}\x{1F1EC}\x{1F1EE}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F7}\x{1F1F8}\x{1F1FB}\x{1F1FE}](?:\x{1F1EA})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1EE}\x{1F1F2}\x{1F1F8}\x{1F1F9}](?:\x{1F1E9})|[\x{1F1E6}\x{1F1E8}\x{1F1EA}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F8}\x{1F1F9}\x{1F1FB}](?:\x{1F1E8})|[\x{1F1E7}\x{1F1EC}\x{1F1F1}\x{1F1F8}](?:\x{1F1E7})|[\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F6}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}\x{1F1FF}](?:\x{1F1E6})|[\x{00A9}\x{00AE}\x{203C}\x{2049}\x{2122}\x{2139}\x{2194}-\x{2199}\x{21A9}-\x{21AA}\x{231A}-\x{231B}\x{2328}\x{23CF}\x{23E9}-\x{23F3}\x{23F8}-\x{23FA}\x{24C2}\x{25AA}-\x{25AB}\x{25B6}\x{25C0}\x{25FB}-\x{25FE}\x{2600}-\x{2604}\x{260E}\x{2611}\x{2614}-\x{2615}\x{2618}\x{261D}\x{2620}\x{2622}-\x{2623}\x{2626}\x{262A}\x{262E}-\x{262F}\x{2638}-\x{263A}\x{2640}\x{2642}\x{2648}-\x{2653}\x{2660}\x{2663}\x{2665}-\x{2666}\x{2668}\x{267B}\x{267E}-\x{267F}\x{2692}-\x{2697}\x{2699}\x{269B}-\x{269C}\x{26A0}-\x{26A1}\x{26AA}-\x{26AB}\x{26B0}-\x{26B1}\x{26BD}-\x{26BE}\x{26C4}-\x{26C5}\x{26C8}\x{26CE}-\x{26CF}\x{26D1}\x{26D3}-\x{26D4}\x{26E9}-\x{26EA}\x{26F0}-\x{26F5}\x{26F7}-\x{26FA}\x{26FD}\x{2702}\x{2705}\x{2708}-\x{270D}\x{270F}\x{2712}\x{2714}\x{2716}\x{271D}\x{2721}\x{2728}\x{2733}-\x{2734}\x{2744}\x{2747}\x{274C}\x{274E}\x{2753}-\x{2755}\x{2757}\x{2763}-\x{2764}\x{2795}-\x{2797}\x{27A1}\x{27B0}\x{27BF}\x{2934}-\x{2935}\x{2B05}-\x{2B07}\x{2B1B}-\x{2B1C}\x{2B50}\x{2B55}\x{3030}\x{303D}\x{3297}\x{3299}\x{1F004}\x{1F0CF}\x{1F170}-\x{1F171}\x{1F17E}-\x{1F17F}\x{1F18E}\x{1F191}-\x{1F19A}\x{1F201}-\x{1F202}\x{1F21A}\x{1F22F}\x{1F232}-\x{1F23A}\x{1F250}-\x{1F251}\x{1F300}-\x{1F321}\x{1F324}-\x{1F393}\x{1F396}-\x{1F397}\x{1F399}-\x{1F39B}\x{1F39E}-\x{1F3F0}\x{1F3F3}-\x{1F3F5}\x{1F3F7}-\x{1F3FA}\x{1F400}-\x{1F4FD}\x{1F4FF}-\x{1F53D}\x{1F549}-\x{1F54E}\x{1F550}-\x{1F567}\x{1F56F}-\x{1F570}\x{1F573}-\x{1F57A}\x{1F587}\x{1F58A}-\x{1F58D}\x{1F590}\x{1F595}-\x{1F596}\x{1F5A4}-\x{1F5A5}\x{1F5A8}\x{1F5B1}-\x{1F5B2}\x{1F5BC}\x{1F5C2}-\x{1F5C4}\x{1F5D1}-\x{1F5D3}\x{1F5DC}-\x{1F5DE}\x{1F5E1}\x{1F5E3}\x{1F5E8}\x{1F5EF}\x{1F5F3}\x{1F5FA}-\x{1F64F}\x{1F680}-\x{1F6C5}\x{1F6CB}-\x{1F6D2}\x{1F6E0}-\x{1F6E5}\x{1F6E9}\x{1F6EB}-\x{1F6EC}\x{1F6F0}\x{1F6F3}-\x{1F6F9}\x{1F910}-\x{1F93A}\x{1F93C}-\x{1F93E}\x{1F940}-\x{1F945}\x{1F947}-\x{1F970}\x{1F973}-\x{1F976}\x{1F97A}\x{1F97C}-\x{1F9A2}\x{1F9B0}-\x{1F9B9}\x{1F9C0}-\x{1F9C2}\x{1F9D0}-\x{1F9FF}]/u', '', $texto);    
    //return preg_replace('', '', $texto);
    }
    public function excesoysize($size, $sizeArchivo) {
        $ecxedesize = 0;
        if($size > 1024) {
            $temp = $size/1024;
            //$temp = number_format($temp, 2, '.', '');
            if ($temp > 1024) {
                $size = number_format($temp/1024, 2, '.', '')." MB";
                if( ($temp/1024) > $sizeArchivo ) {
                    $ecxedesize=1;
                }
            } else {
                $size = number_format($temp, 2, '.', '')." KB";
            }
        } else {
            $size = number_format($size, 2, '.', '')." Bytes";
        }

        return array('size' => $size, 'ecxedesize' => $ecxedesize);
    }

    public function upload_file_post()// damian ok.. se conecta a la otra api
    {
        $userid = $this->utilidades->verifica_ingreso_externo();
        //echo $userid; die(); 
        
        if ($userid == -1) {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        } 
        else 
        { 
	        if($this->input->get_request_header('origen')!==null) 
            {
                $origen = $this->input->get_request_header('origen');
            } 
            else 
            {
	    	   $origen = "eve";
	        }
            //$origen = 'pre';
            if($userid == 1)//aplicacion de externa de nodos
            {
                $hashUserId = 'hK3i6ieOZmhSmKxLtMe6BHFNVqbX3hw1Rp3tIe6/ftD2qaj/4tqMY5hqPp7sNDNyu5z58tUiUG5zPRK0U61iAw==';
            }
            else
            {
                $hashUserId =$this->input->get_request_header('userid');
            }
            //Agregdo para aula virtual
            $id_seccion = $this->input->get_request_header('id-seccion');
            //Fin agregado aula virtual
                
    	    $uploaddir = $this->utilidades->get_url_adjuntos();

            $sizeArchivo=SIZE_EVENT; //MB
            
            $anioA=date('Y');
            $mesA=date('m');

            if(!is_dir($uploaddir)) {
                mkdir($uploaddir, 0777);
            }
            if($origen == 'pre')
            {
                $carpeta = 'preinscripciones/'.$anioA.'-'.$mesA;
                $uploaddir = $uploaddir.$carpeta.'/';

            }
            else
            {
                $carpeta = $anioA.'-'.$mesA;
                $uploaddir = $uploaddir.$carpeta.'/';

            }
                
            if(!is_dir($uploaddir)) {
                mkdir($uploaddir, 0777);
            }

            $data = array();
            
            $error = false;
            $files = array();
           
            foreach ($_FILES as $file) {

                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);  //obtengo la extension
                $extperm = $this->extPermitidas;
                
                if (in_array($ext, $extperm))  {//comprubo que sea una extension permitida
                
                    $datos = array('nombre'=>'', 'origen'=>$origen); // vacio
                    $idfile = $this->modelo_upload->insert_file($datos,$hashUserId); //retorna id insertado

                    //$ultimoId = $this->modelo_upload->obtener_ultimoid() + 1;                

                    $nombre= $idfile."_".$file['name'];
                    $nombre = $this->quitar_emojis($nombre);
                    $nombre = str_replace(" ", "_", $nombre);

                    $nombre2 = $file['name'];
                    $nombre2 = $this->quitar_emojis($nombre2);
                    $nombre2 = str_replace(" ", "_", $nombre2);

                    $tipo = $file['type']; //mime
                    
                    $source = $file['tmp_name'];
                    $source = $this->quitar_emojis($source);
                    $source = str_replace(" ", "_", $source);
                    
		              // Compresión de imágenes
                    $extpermfotos = $this->extPermitidasFotos;

                    if (in_array($ext, $extpermfotos)) {
                        
                        switch($tipo){ 
                            case 'image/jpg':
                                case 'image/jpeg':
                                $nombre = str_ireplace('.jpeg', '.jpg', $nombre);
                                $nombre2 = str_ireplace('.jpeg', '.jpg', $nombre2);
                                $imagen = imagecreatefromjpeg($source); 
                                break; 
                            case 'image/png':
                                $nombre = str_ireplace('.png', '.jpg', $nombre);
                                $nombre2 = str_ireplace('.png', '.jpg', $nombre2);
                                $imagen = imagecreatefrompng($source); 
                                break; 
                            case 'image/gif':
                                $nombre = str_ireplace('.gif', '.jpg', $nombre);
                                $nombre2 = str_ireplace('.gif', '.jpg', $nombre2);
                                $imagen = imagecreatefromgif($source); 
                                break; 
                        }
                        //Guardamos la imagen
                        imagejpeg($imagen, $source, 35);
                    } 
		              // FIN compresión
                    $ruta = $uploaddir.$nombre;
                    //echo $ruta;
                    //echo $source; die();
                    $res = $this->excesoysize($file['size'], $sizeArchivo);
                    $size = $res['size'];
                    $ecxedesize = $res['ecxedesize'];

                    if ($ecxedesize==0) {
                        if($file &&  move_uploaded_file($source, $ruta) ) {
                            // Calcula el tamaño del archivo comprimido
                           // $res = $this->excesoysize(filesize($ruta), $sizeArchivo);
                           // $size2 = $res['size'];
                            $datos = array('nombre'=>$nombre2, 'carpeta'=>$carpeta, 'size'=>$size, 'tipo'=>$tipo); 
                            //Agregado para Aula virtual
                            if($id_seccion != 0) {
                                $datos['evento_id'] = $id_seccion;
                            }
                            //FIN agregado Aula Virtual
                            //Agregado para Aula virtual Alumno
                            //$datos['user_id'] = $userid;

                            //hope hice esto para este caso, el id de la seccion lo conosco y necesito vincularlo a event file... damian
                            if(($origen == 'present_recursos' or $origen == 'present_portada') and $id_seccion > 0)
                            {
                                $array_event_file = array('evento_id' =>$id_seccion, 'file_id' => $idfile);
                                $event_file = $this->modelo_upload->insert_event_file($idfile,$array_event_file,$hashUserId);
                            }
                            $result = $this->modelo_upload->update_file($idfile,$datos,$hashUserId);
                            
                            
                            if($result) {
                                $arre = array('id'=>$idfile, 'nombre'=>$nombre2, 'size'=>$size, 'carpeta'=>$carpeta);
                                $files[] = $arre;  //id file
                            }
                        } else {
                            $error = true;
                            $mensaje="error al subir el archivo"; 
                        }
                    } else {
                        $error = true;
                        $mensaje="tamaño eccedido"; 
                    }
                } else {
                    $error = true;
                    $mensaje="Extensión no permitida";
                }
            }
            //$idsfiles = join(',',$files);
            $data = ($error) ? array('error' => $mensaje, 'nombre'=>$file['name']) : array('files' => $files);   
            print_r( json_encode($data) );
        }   
    }


    public function delete_file_post()// damian ok.. se conecta a la otra api
    {
        $userid = $this->utilidades->verifica_ingreso_externo();
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            $uploaddir = $this->utilidades->get_url_adjuntos();
           
            $idfile = $_POST['idfile'];   
            $idevento = $_POST['idevento'];   

            //asumo que idevento = 0, cuando está subiendo el archivo
            //idevento > 0, cuando el archivo fue subido antes y ahora está editando
            // es decir el evento ya fue creado anteriormente.
            if($userid == 1)//aplicacion de externa de nodos
            {
                $hashUserId = 'hK3i6ieOZmhSmKxLtMe6BHFNVqbX3hw1Rp3tIe6/ftD2qaj/4tqMY5hqPp7sNDNyu5z58tUiUG5zPRK0U61iAw==';
            }
            else
            {
                $hashUserId =$this->input->get_request_header('userid');
            }

            if($idevento == 0){
                $result = $this->modelo_upload->get_file($idfile,$hashUserId); 
                if($result)
                {                
                    $result2 = $this->modelo_upload->delete_file($idfile,$hashUserId); 
                    if($result2)
                    {
                        $name = $result->id .'_'.$result->nombre ;
                        $urlfile =  $uploaddir.$result->carpeta.'/'.$name;
                        //print_r($urlfile);
                        if (file_exists($urlfile))
                            unlink($urlfile);
                    }
                } 
            }
            else
            {
                $this->modelo_upload->delete_event_file($idevento, $idfile,$hashUserId); 

                $files = $this->modelo_upload->get_eventFiles_ByFileId($idfile,$hashUserId);
                                
                if($files==0){
                    $result = $this->modelo_upload->get_file($idfile,$hashUserId); 
                    if($result)
                    {                
                        $result2 = $this->modelo_upload->delete_file($idfile,$hashUserId); 
                        if($result2)
                        {
                            $name = $result->id .'_'.$result->nombre ;
                            $urlfile =  $uploaddir.$result->carpeta.'/'.$name;
                            //print_r($urlfile);
                            if (file_exists($urlfile))
                                unlink($urlfile);
                        }
                    } 
                }
            }

            $this->response([
                'status' => TRUE,
            ], REST_Controller::HTTP_OK); 
               
        }
                    
    }


    public function get_files_get()// no hace falta modificar application\models\Modelo_eventos la llama
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $idevento = $this->get('idevento');

            $uploaddir = $this->utilidades->get_url_adjuntos2();
            
            $result = $this->modelo_upload->get_files_evento($idevento);
            
            if ($result)
            {
                $arre = array();
                
                foreach ($result as $file) {

                    $nombre2 = $file->id.'_'.$file->nombre;
                    
                    $url = $uploaddir.$file->carpeta.'/'.$nombre2;

                    $aux = array(
                        'id' => $file->id,
                        'url' => $url,  
                        'nombre' => $file->nombre,  
                        'tipo' => $file->tipo,   
                        'size' => $file->size
                    );

                    $arre[] = $aux;
                }

                $this->response([
                    'status' => TRUE,
                    'files' => $arre,
                ], REST_Controller::HTTP_OK); 
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron archivos',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    //upload base64
    public function upload_filebase64_post()// damian ok.. se conecta a la otra api
    {
        $userid = $this->utilidades->verifica_ingreso_externo();
        //echo $userid; die(); 
        
        if ($userid == -1) {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        } 
        else 
        { 
            if($this->input->get_request_header('origen')!==null) 
            {
                $origen = $this->input->get_request_header('origen');
            } 
            else 
            {
               $origen = "eve";
            }
            //$origen = 'pre';
            if($userid == 1)//aplicacion de externa de nodos
            {
                $hashUserId = 'hK3i6ieOZmhSmKxLtMe6BHFNVqbX3hw1Rp3tIe6/ftD2qaj/4tqMY5hqPp7sNDNyu5z58tUiUG5zPRK0U61iAw==';
            }
            else
            {
                $hashUserId =$this->input->get_request_header('userid');
            }
            //Agregdo para aula virtual
            $id_seccion = $this->input->get_request_header('id-seccion');
            //Fin agregado aula virtual
                
            $uploaddir = $this->utilidades->get_url_adjuntos();

            $sizeArchivo=SIZE_EVENT; //MB
            
            $anioA=date('Y');
            $mesA=date('m');

            if(!is_dir($uploaddir)) {
                mkdir($uploaddir, 0777);
            }
            if($origen == 'pre')
            {
                $carpeta = 'preinscripciones/'.$anioA.'-'.$mesA;
                $uploaddir = $uploaddir.$carpeta.'/';

            }
            else
            {
                $carpeta = $anioA.'-'.$mesA;
                $uploaddir = $uploaddir.$carpeta.'/';

            }
                
            if(!is_dir($uploaddir)) {
                mkdir($uploaddir, 0777);
            }

            $data = array();
            
            $error = false;
            $files = array();
            $archivobase64 = $_POST["fileBase64"];
            $extensionF = $_POST["type"];
            $extensionF = explode('/', $extensionF);
            $nameFile = $_POST["nombre"];
            $sizeFile = $_POST["size"];
            //comienzo
            $ext = $_POST['ext'];  //obtengo la extension
                $extperm = $this->extPermitidas;
                
                if (in_array($ext, $extperm))  {//comprubo que sea una extension permitida
                
                    $datos = array('nombre'=>'', 'origen'=>$origen); // vacio
                    $idfile = $this->modelo_upload->insert_file($datos,$hashUserId); //retorna id insertado

                    //$ultimoId = $this->modelo_upload->obtener_ultimoid() + 1;                

                    $nombre= $idfile."_".$nameFile;
                    $nombre = $this->quitar_emojis($nombre);
                    $nombre = str_replace(" ", "_", $nombre);

                    $nombre2 = $nameFile;
                    $nombre2 = $this->quitar_emojis($nombre2);
                    $nombre2 = str_replace(" ", "_", $nombre2);

                    $tipo = $_POST["type"]; //mime
                    
                    //$source = $file['tmp_name'];
                    
                    
                      // Compresión de imágenes
                    /*$extpermfotos = $this->extPermitidasFotos;

                    if (in_array($ext, $extpermfotos)) {
                        
                        switch($tipo){ 
                            case 'image/jpg':
                                case 'image/jpeg':
                                $nombre = str_ireplace('.jpeg', '.jpg', $nombre);
                                $nombre2 = str_ireplace('.jpeg', '.jpg', $nombre2);
                                $imagen = imagecreatefromjpeg($source); 
                                break; 
                            case 'image/png':
                                $nombre = str_ireplace('.png', '.jpg', $nombre);
                                $nombre2 = str_ireplace('.png', '.jpg', $nombre2);
                                $imagen = imagecreatefrompng($source); 
                                break; 
                            case 'image/gif':
                                $nombre = str_ireplace('.gif', '.jpg', $nombre);
                                $nombre2 = str_ireplace('.gif', '.jpg', $nombre2);
                                $imagen = imagecreatefromgif($source); 
                                break; 
                        }
                        //Guardamos la imagen
                        imagejpeg($imagen, $source, 35);
                    } */
                      // FIN compresión
                    $ruta = $uploaddir.$nombre;
                    //echo $ruta;
                    //echo $source; die();
                    $res = $this->excesoysize($sizeFile, $sizeArchivo);
                    $size = $res['size'];
                    $ecxedesize = $res['ecxedesize'];

                    if ($ecxedesize==0) {

                        //$ex = $_POST["ex"];
                        $file = fopen($ruta, "wb");
                              
                        $dataf = explode(',', $archivobase64);

                        
                        
                        if( fwrite($file, base64_decode($dataf[1])) ) 
                        {
                            // Calcula el tamaño del archivo comprimido
                           // $res = $this->excesoysize(filesize($ruta), $sizeArchivo);
                           // $size2 = $res['size'];
                            //comprension de fotos
                            //funciona bien, pero se decidio llevar la comprension al controlador upload de nodosweb
                            /*switch($tipo)
                            { 
                                case 'image/jpeg':
                                    
                                    $imagen = imagecreatefromjpeg($ruta); 
                                    //Guardamos la imagen
                                    imagejpeg($imagen, $ruta, 35);
                                    break; 
                                case 'image/jpg':
                                    
                                    $imagen = imagecreatefromjpg($ruta); 
                                    //Guardamos la imagen
                                    imagejpg($imagen, $ruta, 35);
                                    break;
                                case 'image/png':
                                    
                                    $imagen = imagecreatefrompng($ruta); 
                                    imagepng($imagen, $ruta, 9);//de 1 a 9
                                    break; 
                                case 'image/gif':
                                    
                                    $imagen = imagecreatefromgif($ruta); 
                                    imagegif($imagen, $ruta, 35);
                                    break; 
                            }*/
                            
                            //fin comprension
                            $datos = array('nombre'=>$nombre2, 'carpeta'=>$carpeta, 'size'=>$size, 'tipo'=>$tipo); 
                            //Agregado para Aula virtual
                            if($id_seccion != 0) {
                                $datos['evento_id'] = $id_seccion;
                            }
                            //FIN agregado Aula Virtual
                            //Agregado para Aula virtual Alumno
                            //$datos['user_id'] = $userid;

                            //hope hice esto para este caso, el id de la seccion lo conosco y necesito vincularlo a event file... damian
                            if(($origen == 'present_recursos' or $origen == 'present_portada') and $id_seccion > 0)
                            {
                                $array_event_file = array('evento_id' =>$id_seccion, 'file_id' => $idfile);
                                $event_file = $this->modelo_upload->insert_event_file($idfile,$array_event_file,$hashUserId);
                            }
                            $result = $this->modelo_upload->update_file($idfile,$datos,$hashUserId);
                            
                            
                            if($result) {
                                $arre = array('id'=>$idfile, 'nombre'=>$nombre2, 'size'=>$size, 'carpeta'=>$carpeta);
                                $files[] = $arre;  //id file
                            }
                        } else {
                            $error = true;
                            $mensaje="error al subir el archivo"; 
                        }
                        fclose($file);
                    } else {
                        $error = true;
                        $mensaje="tamaño eccedido"; 
                    }
                } else {
                    $error = true;
                    $mensaje="Extensión no permitida";
                }
            $data = ($error) ? array('error' => $mensaje, 'nombre'=>$nameFile, 'type' => $_POST["type"]) : array('files' => $files);   
            print_r( json_encode($data) );
            //fin
        }
    }




    public function subir_foto_post()// damian ok.. se conecta a la otra api
    {
        $userid = $this->utilidades->verifica_userid();
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            $uploaddir = $this->utilidades->get_url_img2();
            //print_r($uploaddir);

            $sizeArchivo=SIZE_FOTO; //MB
            $ecxedesize=0;
            
            $anioA=date('Y');
            $mesA=date('m');

            if(!is_dir($uploaddir)) 
                mkdir($uploaddir, 0777);

            $data = array();
            $error = false;
            $files = array();
           
            foreach($_FILES as $file)
            {
                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);  //obtengo la extension
                $extperm = $this->extPermitidasFotos;
                if (in_array($ext, $extperm))  //compruebo que sea una extension permitida
                {     
                    $nombre0 = 'foto_id'.$userid;
                    $nombre = $nombre0.'.'.$ext;
                    $ruta = $uploaddir.$nombre;
                    $ruta0 = $uploaddir.$nombre0; //sin extension


                    //print_r($ruta);
                    $kb = 0;
                    $size = $file['size'] ;
                    if($size > 1024)
                    {
                        $temp = $size/1024;
                        //$temp = number_format($temp, 2, '.', '');
                        if ($temp > 1024)
                        {
                            $size = number_format($temp/1024, 2, '.', '')." MB";
                            if( ($temp/1024) > $sizeArchivo )
                                $ecxedesize=1;
                        }
                        else
                        {
                            $kb = 1;
                            $size0 = number_format($temp, 2, '.', '');
                            $size = number_format($temp, 2, '.', '')." KB";
                        }
                    }
                    else 
                        $size = number_format($size, 2, '.', '')." Bytes";


                    if($ecxedesize==0)
                    {   
                        if (file_exists($ruta)) {
                            unlink($ruta);
                        }

                        $noAchicar=0;
                        if( ($kb==1) && ($size0 < 500) ) //no comprimir las menores a 500 kb
                            $noAchicar=1;

                        if($file && move_uploaded_file( $file['tmp_name'], $ruta) )
                        //if(0)
                        {
                            if($noAchicar==0)
                            {
                                //creo la imagen gd
                                $ext = pathinfo($ruta, PATHINFO_EXTENSION);
                                $image = $this->crearImagen_gd($ext, $ruta);

                                //obtengo el tamanio original
                                $sizeO = getimagesize($ruta);
                                
                                //comprimo la imagen a la mitad, en forma proporcional al ancho
                                $imageScaled = imagescale($image, $sizeO[0]*0.6); 

                               
                                if (file_exists($ruta)) {
                                    unlink($ruta);
                                }

                                // Save the image as 'simpletext.jpg'
                                //imagejpeg($imageScaled, $ruta.'simpletext.jpg',80); //80 = calidad (del 0 al 100)
                                imagejpeg($imageScaled, $ruta0.'.jpg', 80); //80 = calidad (del 0 al 100)
                                
                                // Free up memory
                                imagedestroy($image);

                                $nombreNew = $nombre0.'.jpg';
                            }
                            else $nombreNew = $nombre;

                            //print_r($nombreNew);

                            $data = array(
                                'foto' => $nombreNew,
                            );

                            $this->modelo_upload->update_foto_usu($data,$userid);
                            
                            $mensaje = "ok"; 
                        }
                        else
                        {
                            $error = true;
                            $mensaje="error al subir el archivo"; 
                        }
                    }
                    else
                        {
                            $error = true;
                            $mensaje="tamaño eccedido"; 
                        }
                }
                else 
                {
                    $error = true;
                    $mensaje="Extensión no permitida";
                }
                    
            }
            //$idsfiles = join(',',$files);
            $data = ($error) ? array('error' => $mensaje) : array('result' => $mensaje,'name' => $nombreNew);
            
            print_r( json_encode($data) );
        }
                    
    }

    public function crearImagen_gd($ext, $src)
    {
        switch ($ext) {
            case 'jpg':
            case 'jpeg':
               $img_r = imagecreatefromjpeg($src);
               break;

             case 'png':
               $img_r = imagecreatefrompng($src);
               break;

             case 'bmp':
               $img_r = imagecreatefromwbmp($src);
               break;
           
           default:
               exit;
               break;
       }
       return $img_r;
    }

    public function recortar_foto_post()// damian ok.. se conecta a la otra api
    {
        $userid = $this->utilidades->verifica_userid();
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                $varX=$_POST['x'];
                $varY=$_POST['y'];
                $varW=$_POST['w'];
                $varH=$_POST['h'];

                $wtotal = $_POST['wtotal'];
                $htotal = $_POST['htotal'];


                //print_r($_POST);

               // $this->load->model("modelo_usuario");
                //$userDatos = $this->modelo_usuario->get_users_id($userid);
                //curl para traer userdatos
                $urlApi = $this->utilidades->get_url_api();
                $apiKey = $this->utilidades->get_apikey();
                $url = $urlApi."uploadbd/get_users_id";
                //echo "<br> $url ";
              //  echo "<br> userid: ".$this->CI->input->get_request_header('userid');
                $ch = curl_init();

                $options = array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_URL => $url,
                    CURLOPT_HTTPHEADER => array("APIKEY: ".$apiKey,"userid: ".$this->input->get_request_header('userid')),
                    CURLOPT_POST => true,
                    //CURLOPT_POSTFIELDS => $ruta,
                    CURLOPT_SSL_VERIFYPEER=> false,
                    );
                curl_setopt_array( $ch, $options);
                $response = curl_exec($ch);
                curl_close($ch);
               // echo "aa: ";print_r($response); echo "aa: ";
                //$response = substr($response, 3);
                $response = json_decode($response);
                //print_r($response);
                //die();
                $userDatos = $response;

                $uploaddir = $this->utilidades->get_url_img2();

           
               //$targ_w = 200;
               //$targ_h = 200;
               $jpeg_quality = 90;
               $src = $uploaddir.$userDatos->foto;

               $nombre = $userDatos->foto;
               $nombre = explode(".", $nombre);

               $src0 = $uploaddir.$nombre[0]; //ruta mas nombre sin extension

               //print_r($src0);

               $sizeO = getimagesize($src);

               $auxW = $wtotal*100/$sizeO[0]; //relacion entre la imagen real y el tamanio del div img
               $auxH = $htotal*100/$sizeO[1]; //relacion entre la imagen real y el tamanio del div img

               $razonW = 100 / $auxW;
               $razonH = 100 / $auxH;


               //print_r($sizeO);

               $ext = pathinfo($src, PATHINFO_EXTENSION);
               
               $img_r = $this->crearImagen_gd($ext, $src);
               

               $varW = $varW*$razonW;
               $varH = $varH*$razonH;

               $varX= $varX*$razonW;
               $varY = $varY*$razonH;

               $targ_w = $varW;
               $targ_h = $varH;

               /*print_r($varW .'x'.$varH);
               print_r('<br>');
               print_r($varX .'x'.$varY);
               print_r('<br>');
               print_r($targ_w .'x'.$targ_h);*/

               
               $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
               //$dst_r = ImageCreateTrueColor($sizeO[0], $sizeO[1]);
               //$dst_r = ImageCreateTrueColor(, $varH*$razonH );
               
               imagecopyresampled($dst_r, $img_r, 0, 0, $varX, $varY, $targ_w, $targ_h, $varW, $varH);
               header('Content-type: image/jpeg');

                if (file_exists($src)) {
                    unlink($src);
                }

               imagejpeg($dst_r, $src0.'.jpg', $jpeg_quality);

               $nombreNew = $nombre[0].'.jpg' ;
               $data = array(
                    'foto' => $nombreNew,
                );

                $this->modelo_upload->update_foto_usu($data,$userid);

               $data = array('name' => $nombreNew);
            
                print_r( json_encode($data) );
            }
        }
                    
    }

    //Agregado para el Aula Virtual
    public function get_seccion_files_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $idevento = $this->get('idevento');
            $uploaddir = $this->utilidades->get_url_adjuntos2();
            
            $origen = $this->get('origen');
            $result = $this->modelo_upload->get_files_seccion($idevento, $origen);
            
            if ($result)
            {
                $arre = array();
                
                foreach ($result as $file) {

                    $nombre2 = $file->id.'_'.$file->nombre;
                    
                    $url = $uploaddir.$file->carpeta.'/'.$nombre2;

                    $aux = array(
                        'id' => $file->id,
                        'url' => $url,  
                        'nombre' => $file->nombre,  
                        'tipo' => $file->tipo,   
                        'size' => $file->size
                    );

                    $arre[] = $aux;
                }

                $this->response([
                    'status' => TRUE,
                    'files' => $arre,
                ], REST_Controller::HTTP_OK); 
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron archivos',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    public function get_prueba_get()
    {
        //echo "hola mundo";
        $userid = $this->utilidades->verifica_userid();
      //  print_r($userid);
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $this->response($userid, REST_Controller::HTTP_OK);
        }
    }
    public function get_almacenamientoOcupadoXcolegioXmes_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            //echo "string";
            //die();
            for($i=1; $i<50; $i++)
            {
                //$idColegio = $_GET['id_colegio'];
                $idColegio = $i;
                $mes = $_GET['mes'];
                //bytes ocupados en eventos
                $sqlEve = "SELECT files.* FROM files
                    JOIN event_file on event_file.file_id = files.id
                    JOIN eventos on eventos.id = event_file.evento_id
                    WHERE files.origen = 'eve'
                    and eventos.colegio_id = $idColegio
                    and files.carpeta = '$mes'";
                $sqlEve = $this->db->query($sqlEve);
                $acumEve = 0;
                $acumEveBytes= 0;
                foreach ($sqlEve->result() as $key ) {
                    # code...
                    $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$mes.'/'.$key->id.'_'.$key->nombre;
                    //echo $ruta." <br>";
                    if (file_exists($ruta))
                    {
                        $acumEve = $acumEve + filesize($ruta);
                        //echo "string";
                    }
                }
                //echo "acumEve: ".$acumEve;
                //echo "string2";
                $unidadEve= '';
                $acumEveBytes = $acumEve;
                if($acumEve > 1025)
                {

                    $acumEve = $acumEve/1024;
                    if($acumEve > 1025)//kb
                    {
                        $acumEve = $acumEve/1024;
                        if($acumEve > 1025)//mb
                        {
                            $acumEve = $acumEve/1024;
                            $unidadEve = 'TBytes';
                            //$acumEve = $acumEve.' TBytes';
                            echo "acumEve: ".$acumEve.' '.$unidadEve;
                        }
                        else
                        {
                            $unidadEve = 'MBytes';
                            //$acumEve = $acumEve.' MBytes';
                            echo "acumEve: ".$acumEve.' '.$unidadEve;;
                        }
                        
                    }
                    else
                    {
                        $unidadEve = 'KBytes';
                        //$acumEve = $acumEve.' KBytes';
                        echo "acumEve: ".$acumEve.' '.$unidadEve;
                    }
                    
                }
                else
                {
                    $unidadEve = 'Bytes';
                    echo "acumEve: ".$acumEve.' '.$unidadEve;
                }
                // COMENTARIOS aula
                echo "<br>";
                //echo "string comentarios";
                $sqlaau = "SELECT event_file.evento_id as idcoment,files.*
                        FROM event_file 
                        JOIN files ON files.id = event_file.file_id
                        JOIN comentarios on comentarios.id = event_file.evento_id
                        JOIN aula_seccion on aula_seccion.id = comentarios.evento_id
                        WHERE files.origen = 'aau'
                        and aula_seccion.id_colegio = $idColegio
                        and files.carpeta = '$mes'
                        AND files.estado = 1";
                //echo "<br> $sqlaau <br>";
                $sqlaau = $this->db->query($sqlaau);

                $acumAau = 0;
                foreach ($sqlaau->result() as $key ) {
                    # code...
                    $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$mes.'/'.$key->id.'_'.$key->nombre;
                    //echo $ruta." <br>";
                    if (file_exists($ruta))
                    {
                        $acumAau = $acumAau + filesize($ruta);
                        //echo "string";
                    }
                }
                //echo "acumEve: ".$acumEve;
                $acumAauBytes = $acumAau;
                $unidadAau = 0;
                if($acumAau > 1025)
                {

                    $acumAau = $acumAau/1024;
                    if($acumAau > 1025)//kb
                    {
                        $acumAau = $acumAau/1024;
                        if($acumAau > 1025)//mb
                        {
                            $unidadAau = 'TBytes';
                            $acumAau = $acumAau/1024;
                            //$acumAau = $acumAau.' TBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "acumauu: ".$acumAau.' '.$unidadAau;
                        }
                        else
                        {
                            $unidadAau = 'MBytes';
                            //$acumAau = $acumAau.' MBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "acumauu: ".$acumAau.' '.$unidadAau;
                        }
                        
                    }
                    else
                    {
                        $unidadAau = 'KBytes';
                        //$acumAau = $acumAau.' KBytes';
                        echo "acumauu: ".$acumAau.' '.$unidadAau;
                    }
                    
                }
                else
                {

                    //$acumAau = $acumAau.' Bytes';
                    $unidadAau = 'Bytes';
                    //echo "acumauu: ".$acumAau;
                    echo "acumauu: ".$acumAau.' '.$unidadAau;
                }
                // comentarios eventos
                echo "<br>";
                $sqlCom = "SELECT event_file.evento_id as idcoment,files.*
                        FROM event_file 
                        JOIN files ON files.id = event_file.file_id
                        JOIN comentarios on comentarios.id = event_file.evento_id
                        JOIN eventos on eventos.id = comentarios.evento_id
                        WHERE files.origen = 'com'
                        and eventos.colegio_id = $idColegio
                        and files.carpeta = '$mes'
                        AND files.estado = 1";
                $sqlCom = $this->db->query($sqlCom);
                
                $acumCom = 0;

                foreach ($sqlCom->result() as $key ) {
                    # code...
                    $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$mes.'/'.$key->id.'_'.$key->nombre;
                    //echo $ruta." <br>";
                    if (file_exists($ruta))
                    {
                        $acumCom = $acumCom + filesize($ruta);
                        //echo "string";
                    }
                }
                $acumComBytes = $acumCom;
                $unidadCom = '';
                if($acumCom > 1025)
                {

                    $acumCom = $acumCom/1024;
                    if($acumCom > 1025)//kb
                    {
                        $acumCom = $acumCom/1024;
                        if($acumCom > 1025)//mb
                        {
                            $unidadCom = 'TBytes';
                            $acumCom = $acumCom/1024;
                            //$acumAau = $acumAau.' TBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "acumCom: ".$acumCom.' '.$unidadCom;
                        }
                        else
                        {
                            $unidadCom = 'MBytes';
                            //$acumAau = $acumAau.' MBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "acumCom: ".$acumCom.' '.$unidadCom;
                        }
                        
                    }
                    else
                    {
                        $unidadCom = 'KBytes';
                        //$acumAau = $acumAau.' KBytes';
                        echo "acumCom: ".$acumCom.' '.$unidadCom;
                    }
                    
                }
                else
                {

                    //$acumAau = $acumAau.' Bytes';
                    $unidadCom = 'Bytes';
                    //echo "acumauu: ".$acumAau;
                    echo "acumCom: ".$acumCom.' '.$unidadCom;
                }
                //archivos aula
                echo "<br>";
                $sqlAula = "SELECT files.* FROM `files` 
                        JOIN aula_seccion on aula_seccion.id = files.evento_id
                        and aula_seccion.id_colegio = $idColegio
                        and files.carpeta = '$mes'";
                $sqlAula = $this->db->query($sqlAula);
                
                $acumAula = 0;
                foreach ($sqlAula->result() as $key ) {
                    # code...
                    $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$mes.'/'.$key->id.'_'.$key->nombre;
                    //echo $ruta." <br>";
                    if (file_exists($ruta))
                    {
                        $acumAula = $acumAula + filesize($ruta);
                        //echo "string";
                    }
                }
                $acumAulaBytes = $acumAula;
                $unidadAula = '';
                if($acumAula > 1025)
                {

                    $acumAula = $acumAula/1024;
                    if($acumAula > 1025)//kb
                    {
                        $acumAula = $acumAula/1024;
                        if($acumAula > 1025)//mb
                        {
                            $unidadAula = 'TBytes';
                            $acumAula = $acumAula/1024;
                            //$acumAau = $acumAau.' TBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "acumAula: ".$acumAula.' '.$unidadAula;
                        }
                        else
                        {
                            $unidadAula = 'MBytes';
                            //$acumAau = $acumAau.' MBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "acumAula: ".$acumAula.' '.$unidadAula;
                        }
                        
                    }
                    else
                    {
                        $unidadAula = 'KBytes';
                        //$acumAau = $acumAau.' KBytes';
                        echo "acumAula: ".$acumAula.' '.$unidadAula;
                    }
                    
                }
                else
                {

                    //$acumAau = $acumAau.' Bytes';
                    $unidadAula = 'Bytes';
                    //echo "acumauu: ".$acumAau;
                    echo "acumAula: ".$acumAula.' '.$unidadAula;
                }
                $bytesTotal = $acumEveBytes + $acumAauBytes +$acumComBytes + $acumAulaBytes;
                $bytesTotalAux = $bytesTotal;
                $unidadTotal = '';
                //total ocupado del colegio
                echo "<br>";
                if($bytesTotalAux > 1025)
                {

                    $bytesTotalAux = $bytesTotalAux/1024;
                    if($bytesTotalAux > 1025)//kb
                    {
                        $bytesTotalAux = $bytesTotalAux/1024;
                        if($bytesTotalAux > 1025)//mb
                        {
                            $unidadTotal = 'TBytes';
                            $bytesTotalAux = $bytesTotalAux/1024;
                            //$acumAau = $acumAau.' TBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "bytesTotalAux: ".$bytesTotalAux.' '.$unidadTotal;
                        }
                        else
                        {
                            $unidadTotal = 'MBytes';
                            //$acumAau = $acumAau.' MBytes';
                            //echo "acumauu: ".$acumAau;
                            echo "bytesTotalAux: ".$bytesTotalAux.' '.$unidadTotal;
                        }
                        
                    }
                    else
                    {
                        $unidadTotal = 'KBytes';
                        //$acumAau = $acumAau.' KBytes';
                        echo "bytesTotalAux: ".$bytesTotalAux.' '.$unidadTotal;
                    }
                    
                }
                else
                {

                    //$acumAau = $acumAau.' Bytes';
                    $unidadTotal = 'Bytes';
                    //echo "acumauu: ".$acumAau;
                    echo "bytesTotalAux: ".$bytesTotalAux.' '.$unidadTotal;
                }
                $array = array(
                    'id_colegio' => $idColegio,
                    'mes' => $mes,
                    'acumEve' => $acumEve,
                    'acumEveBytes' =>$acumEveBytes,
                    'unidadEve' => $unidadEve,
                    'acumAau' => $acumAau,
                    'acumAauBytes' => $acumAauBytes,
                    'unidadAau' => $unidadAau,
                    'acumCom' => $acumCom,
                    'acumComBytes' => $acumComBytes,
                    'unidadCom' => $unidadCom,
                    'acumAula' => $acumAula,
                    'acumAulaBytes' => $acumAulaBytes,
                    'unidadAula' => $unidadAula,
                    'bytesTotal' => $bytesTotal,
                    'bytesTotalAux' => $bytesTotalAux,
                    'unidadTotal' => $unidadTotal
                    );
                print_r($array);

                $sqlReg = "SELECT * FROM `espacioocupadoxcolegio` where mes = '$mes' and id_colegio = $idColegio";
                $sqlReg = $this->db->query($sqlReg);
                if($sqlReg->num_rows() > 0)
                {
                    $array = array(
                    'acumEve' => $acumEve,
                    'acumEveBytes' =>$acumEveBytes,
                    'unidadEve' => $unidadEve,
                    'acumAau' => $acumAau,
                    'acumAauBytes' => $acumAauBytes,
                    'unidadAau' => $unidadAau,
                    'acumCom' => $acumCom,
                    'acumComBytes' => $acumComBytes,
                    'unidadCom' => $unidadCom,
                    'acumAula' => $acumAula,
                    'acumAulaBytes' => $acumAulaBytes,
                    'unidadAula' => $unidadAula,
                    'bytesTotal' => $bytesTotal,
                    'bytesTotalAux' => $bytesTotalAux,
                    'unidadTotal' => $unidadTotal
                    );
                    $this->db->where('id', $sqlReg->row()->id);
                    $this->db->update('espacioocupadoxcolegio',$array);
                }
                else
                {
                    $this->db->insert("espacioocupadoxcolegio",$array);
                }
            }
            //echo json_encode($array);
            //echo "77777777777777777222277222222";
            
        }
    }

    public function get_files_huerfanos_get()
    {
        $userid = $this->utilidades->verifica_userid();
      //  print_r($userid);
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            //$mes = '2020-03';
            $mes = $_GET['mes'];
            $directorio =  $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$mes;
            $ficheros2  = scandir($directorio, 1);
            //print_r($ficheros2);
            $acumBytes = 0;
            $acumBytesTotalMes = 0;
            foreach ($ficheros2 as $file ) {
                $acumBytesTotalMes = $acumBytesTotalMes + filesize($directorio.'/'.$file);
                $fileArray = explode('_', $file);
                $idFile =$fileArray[0];
                //echo $idFile.'<br>';
                if(is_numeric($idFile))
                {
                    $sqlfile = "SELECT * FROM `files` where id = '$idFile'";
                    $sqlfile = $this->db->query($sqlfile);
                    if($sqlfile->num_rows() == 0)
                    {
                        $acumBytes = $acumBytes + filesize($directorio.'/'.$file);
                        echo "<br>";
                        echo "archivo eliminado de tabla files: ".$file;
                        echo "<br>";
                        if (file_exists($directorio.'/'.$file))
                                unlink($directorio.'/'.$file);
                    }
                }
                else
                {
                    if($file != 'index.html' and $file != '.' and $file != '..')
                    {
                        $acumBytes = $acumBytes + filesize($directorio.'/'.$file);
                        echo "<br>";
                        echo "archivo sin indice: ".$file;
                        echo "<br>";
                        if (file_exists($directorio.'/'.$file))
                                unlink($directorio.'/'.$file);
                    }
                        
                }
            }
            echo "<br> el total de bytes acumulados es:".$acumBytes;
            echo "<br> total del mes es: ".$acumBytesTotalMes;
                 
            //$this->response($userid, REST_Controller::HTTP_OK);
        }
    }

    public function get_files_huerfanos_eventos_get()
    {
        $userid = $this->utilidades->verifica_userid();
      //  print_r($userid);
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $mes = $_GET['mes'];
            $directorio =  $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$mes;

            $sqlFiles = "SELECT * FROM `files` WHERE origen = 'eve' and carpeta = '$mes' ORDER BY `files`.`id` DESC";
            $sqlFiles = $this->db->query($sqlFiles);
            foreach ($sqlFiles->result() as $file ) 
            {
                $sqlEventFile = "SELECT * FROM `event_file`
                                WHERE file_id = ".$file->id;
                $sqlEventFile = $this->db->query($sqlEventFile);
                if($sqlEventFile->num_rows() == 0)
                {
                    //el archivo fue adjuntado pero nunca enviado hay q borrarlo a la mierda
                    $ruta = $_SERVER['DOCUMENT_ROOT'].'/administrador_archivos/'.$mes.'/'.$file->id.'_'.$file->nombre;
                    //echo "file: ";
                    echo "ruta: ".$ruta;
                    echo "<br>";
                    if (file_exists($ruta))
                                unlink($ruta);
                }
            }
        }
    }



}
