<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_upload extends CI_Model {

        protected $APIKEY = null;

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();

            $this->APIKEY = $this->utilidades->get_apikey();
        }
        public function update_foto_usu($ruta,$id)
        {
           // $this->db->where("id",$id);
            //$this->db->update("users",$ruta);
            //hay q hacer curl a la api_comunicacion para efectuar la operacion
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/update_foto_usu";
               // echo "<br> $url ";
              //  echo "<br> userid: ".$this->CI->input->get_request_header('userid');
             // echo $ruta
            $ch = curl_init();
            //print_r($ruta);
                $options = array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_URL => $url,
                    CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$this->input->get_request_header('userid')),
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => array('nombreNew' => $ruta['foto']),
                    CURLOPT_SSL_VERIFYPEER=> false,
                    );
                curl_setopt_array( $ch, $options);
                $response = curl_exec($ch);
                curl_close($ch);
               // print_r($response);
                //$response = substr($response, 3);
                $response = json_decode($response);
                return $response;
        }
        public function insert_file($data,$hashUserId)
        {
            //$query = $this->db->insert("files",$data);         
            //return $this->db->insert_id();
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/insert_file";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$hashUserId),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_SSL_VERIFYPEER=> false,
                );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
           // print_r($response);
            //$response = substr($response, 3);
            $response = json_decode($response);
            return $response;
        }
        public function update_file($idfile, $datos,$hashUserId)
        {
            //$this->db->where('id', $idfile);
            //$query = $this->db->update('files', $datos);                   
           // return $query;
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/update_file";
            //echo $url;
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$hashUserId,"idfile: ".$idfile),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $datos,
                CURLOPT_SSL_VERIFYPEER=> false,
                );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //print_r($response);
            //$response = substr($response, 3);
            $response = json_decode($response);
            return $response;
            
        }
        public function insert_event_file($idfile, $datos,$hashUserId)
        {
            //$this->db->where('id', $idfile);
            //$query = $this->db->update('files', $datos);                   
           // return $query;
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/insert_event_file";
            //echo $url;
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$hashUserId,"idfile: ".$idfile),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $datos,
                CURLOPT_SSL_VERIFYPEER=> false,
                );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //print_r($response);
            //$response = substr($response, 3);
            $response = json_decode($response);
            return $response;
            
        }
        public function get_file($idfile,$hashUserId)
        {
            /*$sql = "SELECT * FROM files WHERE id = $idfile LIMIT 1";
            
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return 0;
            }*/
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/get_file";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$hashUserId,"idfile: ".$idfile),
                CURLOPT_POST => true,
                //CURLOPT_POSTFIELDS => $datos,
                CURLOPT_SSL_VERIFYPEER=> false,
                );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //print_r($response);
            //$response = substr($response, 3);
            $response = json_decode($response);
            return $response;

        }
        public function delete_file($idfile,$hashUserId)
        {
            /*$sql = "DELETE FROM files WHERE id = $idfile";
            $query = $this->db->query($sql);
            return $query;*/
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/delete_file";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$hashUserId,"idfile: ".$idfile),
                CURLOPT_POST => true,
                //CURLOPT_POSTFIELDS => $datos,
                CURLOPT_SSL_VERIFYPEER=> false,
                );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //print_r($response);
            //$response = substr($response, 3);
            $response = json_decode($response);
            return $response;
        }
        public function delete_event_file($idevent, $idfile,$hashUserId)
        {
            /*$sql = "DELETE FROM event_file WHERE evento_id = $idevent AND file_id = $idfile";
            $query = $this->db->query($sql);
            return $query;*/
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/delete_event_file";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$hashUserId,"idfile: ".$idfile,"idevent: ".$idevent),
                CURLOPT_POST => true,
                //CURLOPT_POSTFIELDS => $datos,
                CURLOPT_SSL_VERIFYPEER=> false,
                );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //print_r($response);
            //$response = substr($response, 3);
            $response = json_decode($response);
            return $response;
        }
        public function get_eventFiles_ByFileId($idfile,$hashUserId)
        {
            /*$sql = "SELECT * FROM event_file WHERE file_id = $idfile AND estado=1";
            
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return 0;
            }*/
            $urlApi = $this->utilidades->get_url_api();
            $url = $urlApi."uploadbd/get_eventFiles_ByFileId";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: ".$this->APIKEY,"userid: ".$hashUserId,"idfile: ".$idfile),
                CURLOPT_POST => true,
                //CURLOPT_POSTFIELDS => $datos,
                CURLOPT_SSL_VERIFYPEER=> false,
                );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            // print_r($response);
            //$response = substr($response, 3);
            $response = json_decode($response);
            return $response;
        }

	}